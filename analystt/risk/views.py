from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import render
from .models import UplaudsCorpBankcharges
from .serializers import UplaudsCorpBankchargesSerializer


# Create your views here.
@api_view(["GET"])
def index(request, cin):
    if request.method == 'GET':
        # paginator = PageNumberPagination()
        # paginator.page_size = 5   
        ls = UplaudsCorpBankcharges.objects.all().filter(cin=cin)
        # result_page = paginator.paginate_queryset(ls,request)
        serializer = UplaudsCorpBankchargesSerializer(ls, many=True)
        # return paginator.get_paginated_response(serializer.data)
        return Response(serializer.data)
        # return HttpResponse("Hey Django %s" % [y for y in lst])
        # return render(response, 'risk/home.html', {"ls": ls})


def home(request):
   return render(request, 'people-search.html')

# class RiskPagination(LimitOffsetPagination):
#     default_limit = 2
#     max_limit = 3
#
#
# class Riskview(ListAPIView):
#     queryset = UplaudsCorpBankcharges.objects.all()
#     serializer_class = UplaudsCorpBankchargesSerializer
#     filter_backends = (DjangoFilterBackend)
#     filter_fields = ("id","cin")
#     pagination_class = RiskPagination
