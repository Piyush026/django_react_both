from rest_framework import serializers, pagination

from .models import UplaudsCorpBankcharges


class UplaudsCorpBankchargesSerializer(serializers.ModelSerializer):
    class Meta:
        model = UplaudsCorpBankcharges
        fields = '__all__'

# class PaginatedUserSerializer(pagination.PaginationSerializer):
#     """
#     Serializes page objects of user querysets.
#     """
#     class Meta:
#         object_serializer_class = UplaudsCorpBankchargesSerializer
