# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Account(models.Model):
    mvccversion = models.BigIntegerField()
    accountid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentaccountid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    legalname = models.CharField(max_length=75, blank=True, null=True)
    legalid = models.CharField(max_length=75, blank=True, null=True)
    legaltype = models.CharField(max_length=75, blank=True, null=True)
    siccode = models.CharField(max_length=75, blank=True, null=True)
    tickersymbol = models.CharField(max_length=75, blank=True, null=True)
    industry = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    size_field = models.CharField(db_column='size_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'account_'


class Accountentry(models.Model):
    mvccversion = models.BigIntegerField()
    accountentryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentaccountentryid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    domains = models.TextField(blank=True, null=True)
    logoid = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accountentry'


class Accountentryorganizationrel(models.Model):
    mvccversion = models.BigIntegerField()
    accountentryorganizationrelid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    accountentryid = models.BigIntegerField(blank=True, null=True)
    organizationid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accountentryorganizationrel'


class Accountentryuserrel(models.Model):
    mvccversion = models.BigIntegerField()
    accountentryuserrelid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    accountentryid = models.BigIntegerField(blank=True, null=True)
    accountuserid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accountentryuserrel'


class Accountrole(models.Model):
    mvccversion = models.BigIntegerField()
    accountroleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    accountentryid = models.BigIntegerField(blank=True, null=True)
    roleid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accountrole'


class Address(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    addressid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    street1 = models.CharField(max_length=75, blank=True, null=True)
    street2 = models.CharField(max_length=75, blank=True, null=True)
    street3 = models.CharField(max_length=75, blank=True, null=True)
    city = models.CharField(max_length=75, blank=True, null=True)
    zip = models.CharField(max_length=75, blank=True, null=True)
    regionid = models.BigIntegerField(blank=True, null=True)
    countryid = models.BigIntegerField(blank=True, null=True)
    typeid = models.BigIntegerField(blank=True, null=True)
    mailing = models.BooleanField(blank=True, null=True)
    primary_field = models.BooleanField(db_column='primary_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'address'


class Amimageentry(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    amimageentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    configurationuuid = models.CharField(max_length=75, blank=True, null=True)
    fileversionid = models.BigIntegerField(blank=True, null=True)
    mimetype = models.CharField(max_length=75, blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    size_field = models.BigIntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'amimageentry'
        unique_together = (('uuid_field', 'groupid'), ('configurationuuid', 'fileversionid'),)


class Analyticsmessage(models.Model):
    mvccversion = models.BigIntegerField()
    analyticsmessageid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    body = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'analyticsmessage'


class Announcementsdelivery(models.Model):
    mvccversion = models.BigIntegerField()
    deliveryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    email = models.BooleanField(blank=True, null=True)
    sms = models.BooleanField(blank=True, null=True)
    website = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'announcementsdelivery'
        unique_together = (('userid', 'type_field'),)


class Announcementsentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    entryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=75, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    displaydate = models.DateTimeField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    alert = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'announcementsentry'


class Announcementsflag(models.Model):
    mvccversion = models.BigIntegerField()
    flagid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    entryid = models.BigIntegerField(blank=True, null=True)
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'announcementsflag'
        unique_together = (('userid', 'entryid', 'value'),)


class Appbuilderapp(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    appbuilderappid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    ddmstructureid = models.BigIntegerField(blank=True, null=True)
    ddmstructurelayoutid = models.BigIntegerField(blank=True, null=True)
    dedatalistviewid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'appbuilderapp'
        unique_together = (('uuid_field', 'groupid'),)


class Appbuilderappdeployment(models.Model):
    appbuilderappdeploymentid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    appbuilderappid = models.BigIntegerField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'appbuilderappdeployment'


class Assetautotaggerentry(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    assetautotaggerentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetentryid = models.BigIntegerField(blank=True, null=True)
    assettagid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetautotaggerentry'
        unique_together = (('assetautotaggerentryid', 'ctcollectionid'), ('assetentryid', 'assettagid', 'ctcollectionid'),)


class Assetcategory(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    externalreferencecode = models.CharField(max_length=75, blank=True, null=True)
    categoryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentcategoryid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    vocabularyid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetcategory'
        unique_together = (('categoryid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('parentcategoryid', 'name', 'vocabularyid', 'ctcollectionid'),)


class Assetcategoryproperty(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    categorypropertyid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    categoryid = models.BigIntegerField(blank=True, null=True)
    key_field = models.CharField(db_column='key_', max_length=255, blank=True, null=True)  # Field renamed because it ended with '_'.
    value = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetcategoryproperty'
        unique_together = (('categorypropertyid', 'ctcollectionid'), ('categoryid', 'key_field', 'ctcollectionid'),)


class Assetdisplaypageentry(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    assetdisplaypageentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    layoutpagetemplateentryid = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    plid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetdisplaypageentry'
        unique_together = (('assetdisplaypageentryid', 'ctcollectionid'), ('groupid', 'classnameid', 'classpk', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'),)


class AssetentriesAssetcategories(models.Model):
    companyid = models.BigIntegerField()
    categoryid = models.BigIntegerField(primary_key=True)
    entryid = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    ctchangetype = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetentries_assetcategories'
        unique_together = (('categoryid', 'entryid', 'ctcollectionid'),)


class AssetentriesAssettags(models.Model):
    companyid = models.BigIntegerField()
    entryid = models.BigIntegerField(primary_key=True)
    tagid = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    ctchangetype = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetentries_assettags'
        unique_together = (('entryid', 'tagid', 'ctcollectionid'),)


class Assetentry(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    entryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    classuuid = models.CharField(max_length=75, blank=True, null=True)
    classtypeid = models.BigIntegerField(blank=True, null=True)
    listable = models.BooleanField(blank=True, null=True)
    visible = models.BooleanField(blank=True, null=True)
    startdate = models.DateTimeField(blank=True, null=True)
    enddate = models.DateTimeField(blank=True, null=True)
    publishdate = models.DateTimeField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)
    mimetype = models.CharField(max_length=75, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    layoutuuid = models.CharField(max_length=75, blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    priority = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetentry'
        unique_together = (('entryid', 'ctcollectionid'), ('classnameid', 'classpk', 'ctcollectionid'),)


class Assetentryassetcategoryrel(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    assetentryassetcategoryrelid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    assetentryid = models.BigIntegerField(blank=True, null=True)
    assetcategoryid = models.BigIntegerField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetentryassetcategoryrel'
        unique_together = (('assetentryassetcategoryrelid', 'ctcollectionid'),)


class Assetentryusage(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    assetentryusageid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetentryid = models.BigIntegerField(blank=True, null=True)
    containertype = models.BigIntegerField(blank=True, null=True)
    containerkey = models.CharField(max_length=200, blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetentryusage'
        unique_together = (('assetentryusageid', 'ctcollectionid'), ('assetentryid', 'containertype', 'containerkey', 'plid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'),)


class Assetlink(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    linkid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    entryid1 = models.BigIntegerField(blank=True, null=True)
    entryid2 = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    weight = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetlink'
        unique_together = (('linkid', 'ctcollectionid'), ('entryid1', 'entryid2', 'type_field', 'ctcollectionid'),)


class Assetlistentry(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    assetlistentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetlistentrykey = models.CharField(max_length=75, blank=True, null=True)
    title = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetlistentry'
        unique_together = (('assetlistentryid', 'ctcollectionid'), ('groupid', 'assetlistentrykey', 'ctcollectionid'), ('groupid', 'title', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'),)


class Assetlistentryassetentryrel(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    assetlistentryassetentryrelid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetlistentryid = models.BigIntegerField(blank=True, null=True)
    assetentryid = models.BigIntegerField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetlistentryassetentryrel'
        unique_together = (('assetlistentryassetentryrelid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('assetlistentryid', 'segmentsentryid', 'position', 'ctcollectionid'),)


class Assetlistentrysegmentsentryrel(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    alentrysegmentsentryrelid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetlistentryid = models.BigIntegerField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetlistentrysegmentsentryrel'
        unique_together = (('alentrysegmentsentryrelid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('assetlistentryid', 'segmentsentryid', 'ctcollectionid'),)


class Assetlistentryusage(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    assetlistentryusageid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    assetlistentryid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetlistentryusage'
        unique_together = (('assetlistentryusageid', 'ctcollectionid'), ('classnameid', 'classpk', 'portletid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'),)


class Assettag(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    tagid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    assetcount = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assettag'
        unique_together = (('tagid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'name', 'ctcollectionid'),)


class Assetvocabulary(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    externalreferencecode = models.CharField(max_length=75, blank=True, null=True)
    vocabularyid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'assetvocabulary'
        unique_together = (('vocabularyid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'name', 'ctcollectionid'),)


class AuditAuditevent(models.Model):
    auditeventid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    eventtype = models.CharField(max_length=75, blank=True, null=True)
    classname = models.CharField(max_length=200, blank=True, null=True)
    classpk = models.CharField(max_length=75, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    clienthost = models.CharField(max_length=255, blank=True, null=True)
    clientip = models.CharField(max_length=255, blank=True, null=True)
    servername = models.CharField(max_length=255, blank=True, null=True)
    serverport = models.IntegerField(blank=True, null=True)
    sessionid = models.CharField(max_length=255, blank=True, null=True)
    additionalinfo = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'audit_auditevent'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Backgroundtask(models.Model):
    mvccversion = models.BigIntegerField()
    backgroundtaskid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    servletcontextnames = models.CharField(max_length=255, blank=True, null=True)
    taskexecutorclassname = models.CharField(max_length=200, blank=True, null=True)
    taskcontextmap = models.TextField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusmessage = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'backgroundtask'


class Batchengineexporttask(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    batchengineexporttaskid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    callbackurl = models.CharField(max_length=75, blank=True, null=True)
    classname = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)  # This field type is a guess.
    contenttype = models.CharField(max_length=75, blank=True, null=True)
    endtime = models.DateTimeField(blank=True, null=True)
    errormessage = models.CharField(max_length=1000, blank=True, null=True)
    fieldnames = models.CharField(max_length=75, blank=True, null=True)
    executestatus = models.CharField(max_length=75, blank=True, null=True)
    parameters = models.TextField(blank=True, null=True)
    starttime = models.DateTimeField(blank=True, null=True)
    taskitemdelegatename = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'batchengineexporttask'


class Batchengineimporttask(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    batchengineimporttaskid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    batchsize = models.BigIntegerField(blank=True, null=True)
    callbackurl = models.CharField(max_length=75, blank=True, null=True)
    classname = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)  # This field type is a guess.
    contenttype = models.CharField(max_length=75, blank=True, null=True)
    endtime = models.DateTimeField(blank=True, null=True)
    errormessage = models.CharField(max_length=1000, blank=True, null=True)
    executestatus = models.CharField(max_length=75, blank=True, null=True)
    fieldnamemapping = models.TextField(blank=True, null=True)
    operation = models.CharField(max_length=75, blank=True, null=True)
    parameters = models.TextField(blank=True, null=True)
    starttime = models.DateTimeField(blank=True, null=True)
    taskitemdelegatename = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'batchengineimporttask'


class Blogsentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    entryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    subtitle = models.TextField(blank=True, null=True)
    urltitle = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    displaydate = models.DateTimeField(blank=True, null=True)
    allowpingbacks = models.BooleanField(blank=True, null=True)
    allowtrackbacks = models.BooleanField(blank=True, null=True)
    trackbacks = models.TextField(blank=True, null=True)
    coverimagecaption = models.TextField(blank=True, null=True)
    coverimagefileentryid = models.BigIntegerField(blank=True, null=True)
    coverimageurl = models.TextField(blank=True, null=True)
    smallimage = models.BooleanField(blank=True, null=True)
    smallimagefileentryid = models.BigIntegerField(blank=True, null=True)
    smallimageid = models.BigIntegerField(blank=True, null=True)
    smallimageurl = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blogsentry'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'urltitle'),)


class Blogsstatsuser(models.Model):
    mvccversion = models.BigIntegerField()
    statsuserid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    entrycount = models.IntegerField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)
    ratingstotalentries = models.IntegerField(blank=True, null=True)
    ratingstotalscore = models.FloatField(blank=True, null=True)
    ratingsaveragescore = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blogsstatsuser'
        unique_together = (('groupid', 'userid'),)


class Browsertracker(models.Model):
    mvccversion = models.BigIntegerField()
    browsertrackerid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(unique=True, blank=True, null=True)
    browserkey = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'browsertracker'


class Calendar(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    calendarid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    calendarresourceid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    timezoneid = models.CharField(max_length=75, blank=True, null=True)
    color = models.IntegerField(blank=True, null=True)
    defaultcalendar = models.BooleanField(blank=True, null=True)
    enablecomments = models.BooleanField(blank=True, null=True)
    enableratings = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calendar'
        unique_together = (('uuid_field', 'groupid'),)


class Calendarbooking(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    calendarbookingid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    calendarid = models.BigIntegerField(blank=True, null=True)
    calendarresourceid = models.BigIntegerField(blank=True, null=True)
    parentcalendarbookingid = models.BigIntegerField(blank=True, null=True)
    recurringcalendarbookingid = models.BigIntegerField(blank=True, null=True)
    veventuid = models.CharField(max_length=255, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    starttime = models.BigIntegerField(blank=True, null=True)
    endtime = models.BigIntegerField(blank=True, null=True)
    allday = models.BooleanField(blank=True, null=True)
    recurrence = models.TextField(blank=True, null=True)
    firstreminder = models.BigIntegerField(blank=True, null=True)
    firstremindertype = models.CharField(max_length=75, blank=True, null=True)
    secondreminder = models.BigIntegerField(blank=True, null=True)
    secondremindertype = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calendarbooking'
        unique_together = (('calendarid', 'parentcalendarbookingid'), ('calendarid', 'veventuid'), ('uuid_field', 'groupid'),)


class Calendarnotificationtemplate(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    calendarnotificationtemplateid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    calendarid = models.BigIntegerField(blank=True, null=True)
    notificationtype = models.CharField(max_length=75, blank=True, null=True)
    notificationtypesettings = models.CharField(max_length=75, blank=True, null=True)
    notificationtemplatetype = models.CharField(max_length=75, blank=True, null=True)
    subject = models.CharField(max_length=75, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calendarnotificationtemplate'
        unique_together = (('uuid_field', 'groupid'),)


class Calendarresource(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    calendarresourceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    classuuid = models.CharField(max_length=75, blank=True, null=True)
    code_field = models.CharField(db_column='code_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calendarresource'
        unique_together = (('classnameid', 'classpk'), ('uuid_field', 'groupid'),)


class Changesetcollection(models.Model):
    changesetcollectionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changesetcollection'
        unique_together = (('groupid', 'name'),)


class Changesetentry(models.Model):
    changesetentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    changesetcollectionid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changesetentry'
        unique_together = (('changesetcollectionid', 'classnameid', 'classpk'),)


class Classname(models.Model):
    mvccversion = models.BigIntegerField()
    classnameid = models.BigIntegerField(primary_key=True)
    value = models.CharField(unique=True, max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'classname_'


class Company(models.Model):
    mvccversion = models.BigIntegerField()
    companyid = models.BigIntegerField(primary_key=True)
    accountid = models.BigIntegerField(blank=True, null=True)
    webid = models.CharField(unique=True, max_length=75, blank=True, null=True)
    mx = models.CharField(max_length=200, blank=True, null=True)
    homeurl = models.TextField(blank=True, null=True)
    logoid = models.BigIntegerField(blank=True, null=True)
    system_field = models.BooleanField(db_column='system_', blank=True, null=True)  # Field renamed because it ended with '_'.
    maxusers = models.IntegerField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'company'


class Companyinfo(models.Model):
    mvccversion = models.BigIntegerField()
    companyinfoid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(unique=True, blank=True, null=True)
    key_field = models.TextField(db_column='key_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'companyinfo'


class Configuration(models.Model):
    configurationid = models.CharField(primary_key=True, max_length=255)
    dictionary = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'configuration_'


class Contact(models.Model):
    mvccversion = models.BigIntegerField()
    contactid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    accountid = models.BigIntegerField(blank=True, null=True)
    parentcontactid = models.BigIntegerField(blank=True, null=True)
    emailaddress = models.CharField(max_length=254, blank=True, null=True)
    firstname = models.CharField(max_length=75, blank=True, null=True)
    middlename = models.CharField(max_length=75, blank=True, null=True)
    lastname = models.CharField(max_length=75, blank=True, null=True)
    prefixid = models.BigIntegerField(blank=True, null=True)
    suffixid = models.BigIntegerField(blank=True, null=True)
    male = models.BooleanField(blank=True, null=True)
    birthday = models.DateTimeField(blank=True, null=True)
    smssn = models.CharField(max_length=75, blank=True, null=True)
    facebooksn = models.CharField(max_length=75, blank=True, null=True)
    jabbersn = models.CharField(max_length=75, blank=True, null=True)
    skypesn = models.CharField(max_length=75, blank=True, null=True)
    twittersn = models.CharField(max_length=75, blank=True, null=True)
    employeestatusid = models.CharField(max_length=75, blank=True, null=True)
    employeenumber = models.CharField(max_length=75, blank=True, null=True)
    jobtitle = models.CharField(max_length=100, blank=True, null=True)
    jobclass = models.CharField(max_length=75, blank=True, null=True)
    hoursofoperation = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contact_'


class ContactsEntry(models.Model):
    entryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    fullname = models.CharField(max_length=75, blank=True, null=True)
    emailaddress = models.CharField(max_length=254, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contacts_entry'


class Counter(models.Model):
    name = models.CharField(primary_key=True, max_length=150)
    currentid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'counter'


class Country(models.Model):
    mvccversion = models.BigIntegerField()
    countryid = models.BigIntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=75, blank=True, null=True)
    a2 = models.CharField(unique=True, max_length=75, blank=True, null=True)
    a3 = models.CharField(unique=True, max_length=75, blank=True, null=True)
    number_field = models.CharField(db_column='number_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    idd_field = models.CharField(db_column='idd_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    ziprequired = models.BooleanField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'country'


class Ctautoresolutioninfo(models.Model):
    mvccversion = models.BigIntegerField()
    ctautoresolutioninfoid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    ctcollectionid = models.BigIntegerField(blank=True, null=True)
    modelclassnameid = models.BigIntegerField(blank=True, null=True)
    sourcemodelclasspk = models.BigIntegerField(blank=True, null=True)
    targetmodelclasspk = models.BigIntegerField(blank=True, null=True)
    conflictidentifier = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctautoresolutioninfo'


class Ctcollection(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctcollection'


class Ctentry(models.Model):
    mvccversion = models.BigIntegerField()
    ctentryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    ctcollectionid = models.BigIntegerField(blank=True, null=True)
    modelclassnameid = models.BigIntegerField(blank=True, null=True)
    modelclasspk = models.BigIntegerField(blank=True, null=True)
    modelmvccversion = models.BigIntegerField(blank=True, null=True)
    changetype = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctentry'
        unique_together = (('ctcollectionid', 'modelclassnameid', 'modelclasspk'),)


class Ctmessage(models.Model):
    mvccversion = models.BigIntegerField()
    ctmessageid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    ctcollectionid = models.BigIntegerField(blank=True, null=True)
    messagecontent = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctmessage'


class Ctpreferences(models.Model):
    mvccversion = models.BigIntegerField()
    ctpreferencesid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    ctcollectionid = models.BigIntegerField(blank=True, null=True)
    previousctcollectionid = models.BigIntegerField(blank=True, null=True)
    confirmationenabled = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctpreferences'
        unique_together = (('companyid', 'userid'),)


class Ctprocess(models.Model):
    mvccversion = models.BigIntegerField()
    ctprocessid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    ctcollectionid = models.BigIntegerField(blank=True, null=True)
    backgroundtaskid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctprocess'


class Ctscontent(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    ctscontentid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    path_field = models.CharField(db_column='path_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    version = models.CharField(max_length=75, blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'. This field type is a guess.
    size_field = models.BigIntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.
    storetype = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ctscontent'
        unique_together = (('ctscontentid', 'ctcollectionid'), ('companyid', 'repositoryid', 'path_field', 'version', 'ctcollectionid'), ('companyid', 'repositoryid', 'path_field', 'version', 'storetype', 'ctcollectionid'), ('storetype', 'companyid', 'repositoryid', 'path_field', 'version', 'ctcollectionid'),)


class Ddlrecord(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    recordid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    ddmstorageid = models.BigIntegerField(blank=True, null=True)
    recordsetid = models.BigIntegerField(blank=True, null=True)
    recordsetversion = models.CharField(max_length=75, blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    displayindex = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddlrecord'
        unique_together = (('uuid_field', 'groupid'),)


class Ddlrecordset(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    recordsetid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    ddmstructureid = models.BigIntegerField(blank=True, null=True)
    recordsetkey = models.CharField(max_length=75, blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    mindisplayrows = models.IntegerField(blank=True, null=True)
    scope = models.IntegerField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddlrecordset'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'recordsetkey'),)


class Ddlrecordsetversion(models.Model):
    mvccversion = models.BigIntegerField()
    recordsetversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    recordsetid = models.BigIntegerField(blank=True, null=True)
    ddmstructureversionid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    version = models.CharField(max_length=75, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddlrecordsetversion'
        unique_together = (('recordsetid', 'version'),)


class Ddlrecordversion(models.Model):
    mvccversion = models.BigIntegerField()
    recordversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    ddmstorageid = models.BigIntegerField(blank=True, null=True)
    recordsetid = models.BigIntegerField(blank=True, null=True)
    recordsetversion = models.CharField(max_length=75, blank=True, null=True)
    recordid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    displayindex = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddlrecordversion'
        unique_together = (('recordid', 'version'),)


class Ddmcontent(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    contentid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'ddmcontent'
        unique_together = (('uuid_field', 'groupid'),)


class Ddmdataproviderinstance(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    dataproviderinstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'ddmdataproviderinstance'
        unique_together = (('uuid_field', 'groupid'),)


class Ddmdataproviderinstancelink(models.Model):
    mvccversion = models.BigIntegerField()
    dataproviderinstancelinkid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    dataproviderinstanceid = models.BigIntegerField(blank=True, null=True)
    structureid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmdataproviderinstancelink'
        unique_together = (('dataproviderinstanceid', 'structureid'),)


class Ddmforminstance(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    forminstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    structureid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmforminstance'
        unique_together = (('uuid_field', 'groupid'),)


class Ddmforminstancerecord(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    forminstancerecordid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    forminstanceid = models.BigIntegerField(blank=True, null=True)
    forminstanceversion = models.CharField(max_length=75, blank=True, null=True)
    storageid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmforminstancerecord'
        unique_together = (('uuid_field', 'groupid'),)


class Ddmforminstancerecordversion(models.Model):
    mvccversion = models.BigIntegerField()
    forminstancerecordversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    forminstanceid = models.BigIntegerField(blank=True, null=True)
    forminstanceversion = models.CharField(max_length=75, blank=True, null=True)
    forminstancerecordid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    storageid = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmforminstancerecordversion'
        unique_together = (('forminstancerecordid', 'version'),)


class Ddmforminstanceversion(models.Model):
    mvccversion = models.BigIntegerField()
    forminstanceversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    forminstanceid = models.BigIntegerField(blank=True, null=True)
    structureversionid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    version = models.CharField(max_length=75, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmforminstanceversion'
        unique_together = (('forminstanceid', 'version'),)


class Ddmstoragelink(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    storagelinkid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(unique=True, blank=True, null=True)
    structureid = models.BigIntegerField(blank=True, null=True)
    structureversionid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmstoragelink'


class Ddmstructure(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    structureid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentstructureid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    structurekey = models.CharField(max_length=75, blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    storagetype = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmstructure'
        unique_together = (('structureid', 'ctcollectionid'), ('groupid', 'classnameid', 'structurekey', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'),)


class Ddmstructurelayout(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    structurelayoutid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    structurelayoutkey = models.CharField(max_length=75, blank=True, null=True)
    structureversionid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmstructurelayout'
        unique_together = (('groupid', 'classnameid', 'structurelayoutkey'), ('uuid_field', 'groupid'),)


class Ddmstructurelink(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    structurelinkid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    structureid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmstructurelink'
        unique_together = (('structurelinkid', 'ctcollectionid'), ('classnameid', 'classpk', 'structureid', 'ctcollectionid'),)


class Ddmstructureversion(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    structureversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    structureid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    parentstructureid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)
    storagetype = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmstructureversion'
        unique_together = (('structureversionid', 'ctcollectionid'), ('structureid', 'version', 'ctcollectionid'),)


class Ddmtemplate(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    templateid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    versionuserid = models.BigIntegerField(blank=True, null=True)
    versionusername = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    resourceclassnameid = models.BigIntegerField(blank=True, null=True)
    templatekey = models.CharField(max_length=75, blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    mode_field = models.CharField(db_column='mode_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    language = models.CharField(max_length=75, blank=True, null=True)
    script = models.TextField(blank=True, null=True)
    cacheable = models.BooleanField(blank=True, null=True)
    smallimage = models.BooleanField(blank=True, null=True)
    smallimageid = models.BigIntegerField(blank=True, null=True)
    smallimageurl = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmtemplate'
        unique_together = (('templateid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'classnameid', 'templatekey', 'ctcollectionid'),)


class Ddmtemplatelink(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    templatelinkid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    templateid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmtemplatelink'
        unique_together = (('templatelinkid', 'ctcollectionid'), ('classnameid', 'classpk', 'ctcollectionid'),)


class Ddmtemplateversion(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    templateversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    templateid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    language = models.CharField(max_length=75, blank=True, null=True)
    script = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ddmtemplateversion'
        unique_together = (('templateversionid', 'ctcollectionid'), ('templateid', 'version', 'ctcollectionid'),)


class Dedatadefinitionfieldlink(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    dedatadefinitionfieldlinkid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    ddmstructureid = models.BigIntegerField(blank=True, null=True)
    fieldname = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dedatadefinitionfieldlink'
        unique_together = (('classnameid', 'classpk', 'ddmstructureid', 'fieldname'), ('uuid_field', 'groupid'),)


class Dedatalistview(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    dedatalistviewid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    appliedfilters = models.TextField(blank=True, null=True)
    ddmstructureid = models.BigIntegerField(blank=True, null=True)
    fieldnames = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    sortfield = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dedatalistview'
        unique_together = (('uuid_field', 'groupid'),)


class Depotappcustomization(models.Model):
    mvccversion = models.BigIntegerField()
    depotappcustomizationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    depotentryid = models.BigIntegerField(blank=True, null=True)
    enabled = models.BooleanField(blank=True, null=True)
    portletid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'depotappcustomization'
        unique_together = (('depotentryid', 'portletid'),)


class Depotentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    depotentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(unique=True, blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'depotentry'
        unique_together = (('uuid_field', 'groupid'),)


class Depotentrygrouprel(models.Model):
    mvccversion = models.BigIntegerField()
    depotentrygrouprelid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    depotentryid = models.BigIntegerField(blank=True, null=True)
    searchable = models.BooleanField(blank=True, null=True)
    togroupid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'depotentrygrouprel'
        unique_together = (('depotentryid', 'togroupid'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Dlcontent(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    contentid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    path_field = models.CharField(db_column='path_', max_length=255, blank=True, null=True)  # Field renamed because it ended with '_'.
    version = models.CharField(max_length=75, blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'. This field type is a guess.
    size_field = models.BigIntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'dlcontent'
        unique_together = (('contentid', 'ctcollectionid'), ('companyid', 'repositoryid', 'path_field', 'version', 'ctcollectionid'),)


class Dlfileentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fileentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    folderid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    extension = models.CharField(max_length=75, blank=True, null=True)
    mimetype = models.CharField(max_length=75, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    extrasettings = models.TextField(blank=True, null=True)
    fileentrytypeid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    size_field = models.BigIntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.
    smallimageid = models.BigIntegerField(blank=True, null=True)
    largeimageid = models.BigIntegerField(blank=True, null=True)
    custom1imageid = models.BigIntegerField(blank=True, null=True)
    custom2imageid = models.BigIntegerField(blank=True, null=True)
    manualcheckinrequired = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileentry'
        unique_together = (('groupid', 'folderid', 'name'), ('uuid_field', 'groupid'), ('groupid', 'folderid', 'filename'), ('groupid', 'folderid', 'title'),)


class Dlfileentrymetadata(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fileentrymetadataid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    ddmstorageid = models.BigIntegerField(blank=True, null=True)
    ddmstructureid = models.BigIntegerField(blank=True, null=True)
    fileentryid = models.BigIntegerField(blank=True, null=True)
    fileversionid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileentrymetadata'
        unique_together = (('ddmstructureid', 'fileversionid'),)


class Dlfileentrytype(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fileentrytypeid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    fileentrytypekey = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileentrytype'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'fileentrytypekey'),)


class DlfileentrytypesDlfolders(models.Model):
    companyid = models.BigIntegerField()
    fileentrytypeid = models.BigIntegerField(primary_key=True)
    folderid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'dlfileentrytypes_dlfolders'
        unique_together = (('fileentrytypeid', 'folderid'),)


class Dlfilerank(models.Model):
    mvccversion = models.BigIntegerField()
    filerankid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    fileentryid = models.BigIntegerField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'dlfilerank'


class Dlfileshortcut(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fileshortcutid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    folderid = models.BigIntegerField(blank=True, null=True)
    tofileentryid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileshortcut'
        unique_together = (('uuid_field', 'groupid'),)


class Dlfileversion(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fileversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    folderid = models.BigIntegerField(blank=True, null=True)
    fileentryid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    extension = models.CharField(max_length=75, blank=True, null=True)
    mimetype = models.CharField(max_length=75, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    changelog = models.CharField(max_length=75, blank=True, null=True)
    extrasettings = models.TextField(blank=True, null=True)
    fileentrytypeid = models.BigIntegerField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    size_field = models.BigIntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.
    checksum = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileversion'
        unique_together = (('uuid_field', 'groupid'), ('fileentryid', 'version'),)


class Dlfileversionpreview(models.Model):
    dlfileversionpreviewid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    fileentryid = models.BigIntegerField(blank=True, null=True)
    fileversionid = models.BigIntegerField(blank=True, null=True)
    previewstatus = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfileversionpreview'
        unique_together = (('fileentryid', 'fileversionid'),)


class Dlfolder(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    folderid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    mountpoint = models.BooleanField(blank=True, null=True)
    parentfolderid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)
    defaultfileentrytypeid = models.BigIntegerField(blank=True, null=True)
    hidden_field = models.BooleanField(db_column='hidden_', blank=True, null=True)  # Field renamed because it ended with '_'.
    restrictiontype = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlfolder'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'parentfolderid', 'name'),)


class Dlopenerfileentryreference(models.Model):
    dlopenerfileentryreferenceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    referencekey = models.CharField(max_length=75, blank=True, null=True)
    referencetype = models.CharField(max_length=75, blank=True, null=True)
    fileentryid = models.BigIntegerField(unique=True, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'dlopenerfileentryreference'
        unique_together = (('referencetype', 'fileentryid'),)


class Dlsyncevent(models.Model):
    synceventid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    modifiedtime = models.BigIntegerField(blank=True, null=True)
    event = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    typepk = models.BigIntegerField(unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dlsyncevent'


class Emailaddress(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    emailaddressid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    address = models.CharField(max_length=254, blank=True, null=True)
    typeid = models.BigIntegerField(blank=True, null=True)
    primary_field = models.BooleanField(db_column='primary_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'emailaddress'


class Expandocolumn(models.Model):
    columnid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    tableid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    defaultdata = models.TextField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'expandocolumn'
        unique_together = (('tableid', 'name'),)


class Expandorow(models.Model):
    rowid_field = models.BigIntegerField(db_column='rowid_', primary_key=True)  # Field renamed because it ended with '_'.
    companyid = models.BigIntegerField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    tableid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'expandorow'
        unique_together = (('tableid', 'classpk'),)


class Expandotable(models.Model):
    tableid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'expandotable'
        unique_together = (('companyid', 'classnameid', 'name'),)


class Expandovalue(models.Model):
    valueid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    tableid = models.BigIntegerField(blank=True, null=True)
    columnid = models.BigIntegerField(blank=True, null=True)
    rowid_field = models.BigIntegerField(db_column='rowid_', blank=True, null=True)  # Field renamed because it ended with '_'.
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'expandovalue'
        unique_together = (('columnid', 'rowid_field'), ('tableid', 'columnid', 'classpk'),)


class Exportimportconfiguration(models.Model):
    mvccversion = models.BigIntegerField()
    exportimportconfigurationid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'exportimportconfiguration'


class Fragmentcollection(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fragmentcollectionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    fragmentcollectionkey = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fragmentcollection'
        unique_together = (('groupid', 'fragmentcollectionkey'), ('uuid_field', 'groupid'),)


class Fragmentcomposition(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fragmentcompositionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    fragmentcollectionid = models.BigIntegerField(blank=True, null=True)
    fragmentcompositionkey = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.CharField(max_length=75, blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'.
    previewfileentryid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fragmentcomposition'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'fragmentcompositionkey'),)


class Fragmententry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fragmententryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    fragmentcollectionid = models.BigIntegerField(blank=True, null=True)
    fragmententrykey = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    html = models.TextField(blank=True, null=True)
    js = models.TextField(blank=True, null=True)
    cacheable = models.BooleanField(blank=True, null=True)
    configuration = models.TextField(blank=True, null=True)
    previewfileentryid = models.BigIntegerField(blank=True, null=True)
    readonly = models.BooleanField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fragmententry'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'fragmententrykey'),)


class Fragmententrylink(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fragmententrylinkid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    originalfragmententrylinkid = models.BigIntegerField(blank=True, null=True)
    fragmententryid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    html = models.TextField(blank=True, null=True)
    js = models.TextField(blank=True, null=True)
    configuration = models.TextField(blank=True, null=True)
    editablevalues = models.TextField(blank=True, null=True)
    namespace = models.CharField(max_length=75, blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)
    rendererkey = models.CharField(max_length=200, blank=True, null=True)
    lastpropagationdate = models.DateTimeField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fragmententrylink'
        unique_together = (('uuid_field', 'groupid'),)


class Friendlyurlentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    defaultlanguageid = models.CharField(max_length=75, blank=True, null=True)
    friendlyurlentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'friendlyurlentry'
        unique_together = (('uuid_field', 'groupid'),)


class Friendlyurlentrylocalization(models.Model):
    mvccversion = models.BigIntegerField()
    friendlyurlentrylocalizationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    friendlyurlentryid = models.BigIntegerField(blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)
    urltitle = models.CharField(max_length=255, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'friendlyurlentrylocalization'
        unique_together = (('friendlyurlentryid', 'languageid'), ('groupid', 'classnameid', 'urltitle'),)


class Friendlyurlentrymapping(models.Model):
    mvccversion = models.BigIntegerField()
    friendlyurlentrymappingid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    friendlyurlentryid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'friendlyurlentrymapping'
        unique_together = (('classnameid', 'classpk'),)


class Group(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    creatoruserid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    parentgroupid = models.BigIntegerField(blank=True, null=True)
    livegroupid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    groupkey = models.CharField(max_length=150, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    typesettings = models.TextField(blank=True, null=True)
    manualmembership = models.BooleanField(blank=True, null=True)
    membershiprestriction = models.IntegerField(blank=True, null=True)
    friendlyurl = models.CharField(max_length=255, blank=True, null=True)
    site = models.BooleanField(blank=True, null=True)
    remotestaginggroupcount = models.IntegerField(blank=True, null=True)
    inheritcontent = models.BooleanField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'group_'
        unique_together = (('companyid', 'friendlyurl'), ('uuid_field', 'groupid'), ('companyid', 'classnameid', 'livegroupid', 'groupkey'), ('companyid', 'livegroupid', 'groupkey'), ('companyid', 'groupkey'), ('companyid', 'classnameid', 'classpk'),)


class GroupsOrgs(models.Model):
    companyid = models.BigIntegerField()
    groupid = models.BigIntegerField(primary_key=True)
    organizationid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'groups_orgs'
        unique_together = (('groupid', 'organizationid'),)


class GroupsRoles(models.Model):
    companyid = models.BigIntegerField()
    groupid = models.BigIntegerField(primary_key=True)
    roleid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'groups_roles'
        unique_together = (('groupid', 'roleid'),)


class GroupsUsergroups(models.Model):
    companyid = models.BigIntegerField()
    groupid = models.BigIntegerField(primary_key=True)
    usergroupid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'groups_usergroups'
        unique_together = (('groupid', 'usergroupid'),)


class Htmlpreviewentry(models.Model):
    htmlpreviewentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    fileentryid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'htmlpreviewentry'


class ImMemberrequest(models.Model):
    memberrequestid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    key_field = models.CharField(db_column='key_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    receiveruserid = models.BigIntegerField(blank=True, null=True)
    invitedroleid = models.BigIntegerField(blank=True, null=True)
    invitedteamid = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'im_memberrequest'


class Image(models.Model):
    mvccversion = models.BigIntegerField()
    imageid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    size_field = models.IntegerField(db_column='size_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'image'


class Journalarticle(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    resourceprimkey = models.BigIntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    folderid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    articleid = models.CharField(max_length=75, blank=True, null=True)
    version = models.FloatField(blank=True, null=True)
    urltitle = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    ddmstructurekey = models.CharField(max_length=75, blank=True, null=True)
    ddmtemplatekey = models.CharField(max_length=75, blank=True, null=True)
    defaultlanguageid = models.CharField(max_length=75, blank=True, null=True)
    layoutuuid = models.CharField(max_length=75, blank=True, null=True)
    displaydate = models.DateTimeField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)
    reviewdate = models.DateTimeField(blank=True, null=True)
    indexable = models.BooleanField(blank=True, null=True)
    smallimage = models.BooleanField(blank=True, null=True)
    smallimageid = models.BigIntegerField(blank=True, null=True)
    smallimageurl = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalarticle'
        unique_together = (('id_field', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'articleid', 'version', 'ctcollectionid'),)


class Journalarticlelocalization(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    articlelocalizationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    articlepk = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=400, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalarticlelocalization'
        unique_together = (('articlelocalizationid', 'ctcollectionid'), ('articlepk', 'languageid', 'ctcollectionid'), ('companyid', 'articlepk', 'languageid', 'ctcollectionid'),)


class Journalarticleresource(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    resourceprimkey = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    articleid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalarticleresource'
        unique_together = (('resourceprimkey', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'articleid', 'ctcollectionid'),)


class Journalcontentsearch(models.Model):
    mvccversion = models.BigIntegerField()
    contentsearchid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    layoutid = models.BigIntegerField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    articleid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalcontentsearch'
        unique_together = (('groupid', 'privatelayout', 'layoutid', 'portletid', 'articleid'),)


class Journalfeed(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    feedid = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    ddmstructurekey = models.CharField(max_length=75, blank=True, null=True)
    ddmtemplatekey = models.CharField(max_length=75, blank=True, null=True)
    ddmrenderertemplatekey = models.CharField(max_length=75, blank=True, null=True)
    delta = models.IntegerField(blank=True, null=True)
    orderbycol = models.CharField(max_length=75, blank=True, null=True)
    orderbytype = models.CharField(max_length=75, blank=True, null=True)
    targetlayoutfriendlyurl = models.CharField(max_length=255, blank=True, null=True)
    targetportletid = models.CharField(max_length=200, blank=True, null=True)
    contentfield = models.CharField(max_length=75, blank=True, null=True)
    feedformat = models.CharField(max_length=75, blank=True, null=True)
    feedversion = models.FloatField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalfeed'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'feedid'),)


class Journalfolder(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    folderid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentfolderid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    restrictiontype = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'journalfolder'
        unique_together = (('folderid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'parentfolderid', 'name', 'ctcollectionid'),)


class Kaleoaction(models.Model):
    mvccversion = models.BigIntegerField()
    kaleoactionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodename = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    executiontype = models.CharField(max_length=20, blank=True, null=True)
    script = models.TextField(blank=True, null=True)
    scriptlanguage = models.CharField(max_length=75, blank=True, null=True)
    scriptrequiredcontexts = models.TextField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleoaction'


class Kaleocondition(models.Model):
    mvccversion = models.BigIntegerField()
    kaleoconditionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodeid = models.BigIntegerField(blank=True, null=True)
    script = models.TextField(blank=True, null=True)
    scriptlanguage = models.CharField(max_length=75, blank=True, null=True)
    scriptrequiredcontexts = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleocondition'


class Kaleodefinition(models.Model):
    mvccversion = models.BigIntegerField()
    kaleodefinitionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    version = models.IntegerField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'kaleodefinition'


class Kaleodefinitionversion(models.Model):
    mvccversion = models.BigIntegerField()
    kaleodefinitionversionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    startkaleonodeid = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleodefinitionversion'
        unique_together = (('companyid', 'name', 'version'),)


class Kaleoinstance(models.Model):
    mvccversion = models.BigIntegerField()
    kaleoinstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionname = models.CharField(max_length=200, blank=True, null=True)
    kaleodefinitionversion = models.IntegerField(blank=True, null=True)
    rootkaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    classname = models.CharField(max_length=200, blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)
    workflowcontext = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleoinstance'


class Kaleoinstancetoken(models.Model):
    mvccversion = models.BigIntegerField()
    kaleoinstancetokenid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    parentkaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    currentkaleonodeid = models.BigIntegerField(blank=True, null=True)
    currentkaleonodename = models.CharField(max_length=200, blank=True, null=True)
    classname = models.CharField(max_length=200, blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleoinstancetoken'


class Kaleolog(models.Model):
    mvccversion = models.BigIntegerField()
    kaleologid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    kaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleonodename = models.CharField(max_length=200, blank=True, null=True)
    terminalkaleonode = models.BooleanField(blank=True, null=True)
    kaleoactionid = models.BigIntegerField(blank=True, null=True)
    kaleoactionname = models.CharField(max_length=200, blank=True, null=True)
    kaleoactiondescription = models.TextField(blank=True, null=True)
    previouskaleonodeid = models.BigIntegerField(blank=True, null=True)
    previouskaleonodename = models.CharField(max_length=200, blank=True, null=True)
    previousassigneeclassname = models.CharField(max_length=200, blank=True, null=True)
    previousassigneeclasspk = models.BigIntegerField(blank=True, null=True)
    currentassigneeclassname = models.CharField(max_length=200, blank=True, null=True)
    currentassigneeclasspk = models.BigIntegerField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=50, blank=True, null=True)  # Field renamed because it ended with '_'.
    comment_field = models.TextField(db_column='comment_', blank=True, null=True)  # Field renamed because it ended with '_'.
    startdate = models.DateTimeField(blank=True, null=True)
    enddate = models.DateTimeField(blank=True, null=True)
    duration = models.BigIntegerField(blank=True, null=True)
    workflowcontext = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleolog'


class Kaleonode(models.Model):
    mvccversion = models.BigIntegerField()
    kaleonodeid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    metadata = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=20, blank=True, null=True)  # Field renamed because it ended with '_'.
    initial_field = models.BooleanField(db_column='initial_', blank=True, null=True)  # Field renamed because it ended with '_'.
    terminal = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleonode'


class Kaleonotification(models.Model):
    mvccversion = models.BigIntegerField()
    kaleonotificationid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodename = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    executiontype = models.CharField(max_length=20, blank=True, null=True)
    template = models.TextField(blank=True, null=True)
    templatelanguage = models.CharField(max_length=75, blank=True, null=True)
    notificationtypes = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleonotification'


class Kaleonotificationrecipient(models.Model):
    mvccversion = models.BigIntegerField()
    kaleonotificationrecipientid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonotificationid = models.BigIntegerField(blank=True, null=True)
    recipientclassname = models.CharField(max_length=200, blank=True, null=True)
    recipientclasspk = models.BigIntegerField(blank=True, null=True)
    recipientroletype = models.IntegerField(blank=True, null=True)
    recipientscript = models.TextField(blank=True, null=True)
    recipientscriptlanguage = models.CharField(max_length=75, blank=True, null=True)
    recipientscriptcontexts = models.TextField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    notificationreceptiontype = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleonotificationrecipient'


class Kaleotask(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodeid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotask'


class Kaleotaskassignment(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskassignmentid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodeid = models.BigIntegerField(blank=True, null=True)
    assigneeclassname = models.CharField(max_length=200, blank=True, null=True)
    assigneeclasspk = models.BigIntegerField(blank=True, null=True)
    assigneeactionid = models.CharField(max_length=75, blank=True, null=True)
    assigneescript = models.TextField(blank=True, null=True)
    assigneescriptlanguage = models.CharField(max_length=75, blank=True, null=True)
    assigneescriptrequiredcontexts = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotaskassignment'


class Kaleotaskassignmentinstance(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskassignmentinstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    kaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskid = models.BigIntegerField(blank=True, null=True)
    kaleotaskname = models.CharField(max_length=200, blank=True, null=True)
    assigneeclassname = models.CharField(max_length=200, blank=True, null=True)
    assigneeclasspk = models.BigIntegerField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotaskassignmentinstance'


class Kaleotaskform(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskformid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodeid = models.BigIntegerField(blank=True, null=True)
    kaleotaskid = models.BigIntegerField(blank=True, null=True)
    kaleotaskname = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    formcompanyid = models.BigIntegerField(blank=True, null=True)
    formdefinition = models.TextField(blank=True, null=True)
    formgroupid = models.BigIntegerField(blank=True, null=True)
    formid = models.BigIntegerField(blank=True, null=True)
    formuuid = models.CharField(max_length=75, blank=True, null=True)
    metadata = models.TextField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotaskform'


class Kaleotaskforminstance(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskforminstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    kaleotaskid = models.BigIntegerField(blank=True, null=True)
    kaleotaskinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskformid = models.BigIntegerField(blank=True, null=True)
    formvalues = models.TextField(blank=True, null=True)
    formvalueentrygroupid = models.BigIntegerField(blank=True, null=True)
    formvalueentryid = models.BigIntegerField(blank=True, null=True)
    formvalueentryuuid = models.CharField(max_length=75, blank=True, null=True)
    metadata = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotaskforminstance'


class Kaleotaskinstancetoken(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotaskinstancetokenid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    kaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskid = models.BigIntegerField(blank=True, null=True)
    kaleotaskname = models.CharField(max_length=200, blank=True, null=True)
    classname = models.CharField(max_length=200, blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    completionuserid = models.BigIntegerField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)
    duedate = models.DateTimeField(blank=True, null=True)
    workflowcontext = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotaskinstancetoken'


class Kaleotimer(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotimerid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    blocking = models.BooleanField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    duration = models.FloatField(blank=True, null=True)
    scale = models.CharField(max_length=75, blank=True, null=True)
    recurrenceduration = models.FloatField(blank=True, null=True)
    recurrencescale = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotimer'


class Kaleotimerinstancetoken(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotimerinstancetokenid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleoclassname = models.CharField(max_length=200, blank=True, null=True)
    kaleoclasspk = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleoinstanceid = models.BigIntegerField(blank=True, null=True)
    kaleoinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotaskinstancetokenid = models.BigIntegerField(blank=True, null=True)
    kaleotimerid = models.BigIntegerField(blank=True, null=True)
    kaleotimername = models.CharField(max_length=200, blank=True, null=True)
    blocking = models.BooleanField(blank=True, null=True)
    completionuserid = models.BigIntegerField(blank=True, null=True)
    completed = models.BooleanField(blank=True, null=True)
    completiondate = models.DateTimeField(blank=True, null=True)
    workflowcontext = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotimerinstancetoken'


class Kaleotransition(models.Model):
    mvccversion = models.BigIntegerField()
    kaleotransitionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    kaleodefinitionid = models.BigIntegerField(blank=True, null=True)
    kaleodefinitionversionid = models.BigIntegerField(blank=True, null=True)
    kaleonodeid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    sourcekaleonodeid = models.BigIntegerField(blank=True, null=True)
    sourcekaleonodename = models.CharField(max_length=200, blank=True, null=True)
    targetkaleonodeid = models.BigIntegerField(blank=True, null=True)
    targetkaleonodename = models.CharField(max_length=200, blank=True, null=True)
    defaulttransition = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kaleotransition'


class Kbarticle(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    kbarticleid = models.BigIntegerField(primary_key=True)
    resourceprimkey = models.BigIntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    rootresourceprimkey = models.BigIntegerField(blank=True, null=True)
    parentresourceclassnameid = models.BigIntegerField(blank=True, null=True)
    parentresourceprimkey = models.BigIntegerField(blank=True, null=True)
    kbfolderid = models.BigIntegerField(blank=True, null=True)
    version = models.IntegerField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    urltitle = models.CharField(max_length=75, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    priority = models.FloatField(blank=True, null=True)
    sections = models.TextField(blank=True, null=True)
    latest = models.BooleanField(blank=True, null=True)
    main = models.BooleanField(blank=True, null=True)
    sourceurl = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kbarticle'
        unique_together = (('uuid_field', 'groupid'), ('resourceprimkey', 'version'), ('resourceprimkey', 'groupid', 'version'),)


class Kbcomment(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    kbcommentid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    userrating = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kbcomment'
        unique_together = (('uuid_field', 'groupid'),)


class Kbfolder(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    kbfolderid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentkbfolderid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    urltitle = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kbfolder'
        unique_together = (('uuid_field', 'groupid'),)


class Kbtemplate(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    kbtemplateid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kbtemplate'
        unique_together = (('uuid_field', 'groupid'),)


class Layout(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    plid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentplid = models.BigIntegerField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    layoutid = models.BigIntegerField(blank=True, null=True)
    parentlayoutid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    robots = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    typesettings = models.TextField(blank=True, null=True)
    hidden_field = models.BooleanField(db_column='hidden_', blank=True, null=True)  # Field renamed because it ended with '_'.
    system_field = models.BooleanField(db_column='system_', blank=True, null=True)  # Field renamed because it ended with '_'.
    friendlyurl = models.CharField(max_length=255, blank=True, null=True)
    iconimageid = models.BigIntegerField(blank=True, null=True)
    themeid = models.CharField(max_length=75, blank=True, null=True)
    colorschemeid = models.CharField(max_length=75, blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    masterlayoutplid = models.BigIntegerField(blank=True, null=True)
    layoutprototypeuuid = models.CharField(max_length=75, blank=True, null=True)
    layoutprototypelinkenabled = models.BooleanField(blank=True, null=True)
    sourceprototypelayoutuuid = models.CharField(max_length=75, blank=True, null=True)
    publishdate = models.DateTimeField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layout'
        unique_together = (('plid', 'ctcollectionid'), ('uuid_field', 'groupid', 'privatelayout', 'ctcollectionid'), ('groupid', 'privatelayout', 'friendlyurl', 'ctcollectionid'), ('groupid', 'privatelayout', 'layoutid', 'ctcollectionid'),)


class Layoutbranch(models.Model):
    mvccversion = models.BigIntegerField()
    layoutbranchid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    layoutsetbranchid = models.BigIntegerField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    master = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutbranch'
        unique_together = (('layoutsetbranchid', 'plid', 'name'),)


class Layoutclassedmodelusage(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutclassedmodelusageid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    containerkey = models.CharField(max_length=200, blank=True, null=True)
    containertype = models.BigIntegerField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutclassedmodelusage'
        unique_together = (('uuid_field', 'groupid'), ('classnameid', 'classpk', 'containerkey', 'containertype', 'plid'),)


class Layoutfriendlyurl(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutfriendlyurlid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    friendlyurl = models.CharField(max_length=255, blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutfriendlyurl'
        unique_together = (('layoutfriendlyurlid', 'ctcollectionid'), ('plid', 'languageid', 'ctcollectionid'), ('uuid_field', 'groupid', 'ctcollectionid'), ('groupid', 'privatelayout', 'friendlyurl', 'languageid', 'ctcollectionid'),)


class Layoutpagetemplatecollection(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutpagetemplatecollectionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    lptcollectionkey = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutpagetemplatecollection'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'name'), ('groupid', 'lptcollectionkey'),)


class Layoutpagetemplateentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutpagetemplateentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    layoutpagetemplatecollectionid = models.BigIntegerField(blank=True, null=True)
    layoutpagetemplateentrykey = models.CharField(max_length=75, blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classtypeid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    previewfileentryid = models.BigIntegerField(blank=True, null=True)
    defaulttemplate = models.BooleanField(blank=True, null=True)
    layoutprototypeid = models.BigIntegerField(blank=True, null=True)
    plid = models.BigIntegerField(unique=True, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutpagetemplateentry'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'name', 'type_field'), ('groupid', 'layoutpagetemplateentrykey'),)


class Layoutpagetemplatestructure(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutpagetemplatestructureid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutpagetemplatestructure'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'classnameid', 'classpk'),)


class Layoutpagetemplatestructurerel(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    lpagetemplatestructurerelid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    layoutpagetemplatestructureid = models.BigIntegerField(blank=True, null=True)
    segmentsexperienceid = models.BigIntegerField(blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'layoutpagetemplatestructurerel'
        unique_together = (('uuid_field', 'groupid'), ('layoutpagetemplatestructureid', 'segmentsexperienceid'),)


class Layoutprototype(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutprototypeid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'layoutprototype'


class Layoutrevision(models.Model):
    mvccversion = models.BigIntegerField()
    layoutrevisionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    layoutsetbranchid = models.BigIntegerField(blank=True, null=True)
    layoutbranchid = models.BigIntegerField(blank=True, null=True)
    parentlayoutrevisionid = models.BigIntegerField(blank=True, null=True)
    head = models.BooleanField(blank=True, null=True)
    major = models.BooleanField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    robots = models.TextField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    iconimageid = models.BigIntegerField(blank=True, null=True)
    themeid = models.CharField(max_length=75, blank=True, null=True)
    colorschemeid = models.CharField(max_length=75, blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutrevision'


class Layoutseoentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutseoentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    layoutid = models.BigIntegerField(blank=True, null=True)
    canonicalurl = models.TextField(blank=True, null=True)
    canonicalurlenabled = models.BooleanField(blank=True, null=True)
    ddmstorageid = models.BigIntegerField(blank=True, null=True)
    opengraphdescription = models.TextField(blank=True, null=True)
    opengraphdescriptionenabled = models.BooleanField(blank=True, null=True)
    opengraphimagealt = models.TextField(blank=True, null=True)
    opengraphimagefileentryid = models.BigIntegerField(blank=True, null=True)
    opengraphtitle = models.TextField(blank=True, null=True)
    opengraphtitleenabled = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutseoentry'
        unique_together = (('groupid', 'privatelayout', 'layoutid'), ('uuid_field', 'groupid'),)


class Layoutseosite(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutseositeid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(unique=True, blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    opengraphenabled = models.BooleanField(blank=True, null=True)
    opengraphimagealt = models.TextField(blank=True, null=True)
    opengraphimagefileentryid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutseosite'
        unique_together = (('uuid_field', 'groupid'),)


class Layoutset(models.Model):
    mvccversion = models.BigIntegerField()
    layoutsetid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    logoid = models.BigIntegerField(blank=True, null=True)
    themeid = models.CharField(max_length=75, blank=True, null=True)
    colorschemeid = models.CharField(max_length=75, blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutsetprototypeuuid = models.CharField(max_length=75, blank=True, null=True)
    layoutsetprototypelinkenabled = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutset'
        unique_together = (('groupid', 'privatelayout'),)


class Layoutsetbranch(models.Model):
    mvccversion = models.BigIntegerField()
    layoutsetbranchid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    privatelayout = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    master = models.BooleanField(blank=True, null=True)
    logoid = models.BigIntegerField(blank=True, null=True)
    themeid = models.CharField(max_length=75, blank=True, null=True)
    colorschemeid = models.CharField(max_length=75, blank=True, null=True)
    css = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutsetprototypeuuid = models.CharField(max_length=75, blank=True, null=True)
    layoutsetprototypelinkenabled = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layoutsetbranch'
        unique_together = (('groupid', 'privatelayout', 'name'),)


class Layoutsetprototype(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    layoutsetprototypeid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    settings_field = models.TextField(db_column='settings_', blank=True, null=True)  # Field renamed because it ended with '_'.
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'layoutsetprototype'


class Listtype(models.Model):
    mvccversion = models.BigIntegerField()
    listtypeid = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'listtype'


class Lock(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    lockid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    classname = models.CharField(max_length=75, blank=True, null=True)
    key_field = models.CharField(db_column='key_', max_length=255, blank=True, null=True)  # Field renamed because it ended with '_'.
    owner = models.CharField(max_length=1024, blank=True, null=True)
    inheritable = models.BooleanField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lock_'
        unique_together = (('classname', 'key_field'),)


class MarketplaceApp(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    appid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    remoteappid = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    category = models.CharField(max_length=75, blank=True, null=True)
    iconurl = models.TextField(blank=True, null=True)
    version = models.CharField(max_length=75, blank=True, null=True)
    required = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marketplace_app'


class MarketplaceModule(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    moduleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    appid = models.BigIntegerField(blank=True, null=True)
    bundlesymbolicname = models.CharField(max_length=500, blank=True, null=True)
    bundleversion = models.CharField(max_length=75, blank=True, null=True)
    contextname = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marketplace_module'


class Mbban(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    banid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    banuserid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbban'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'banuserid'),)


class Mbcategory(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    categoryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentcategoryid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    displaystyle = models.CharField(max_length=75, blank=True, null=True)
    threadcount = models.IntegerField(blank=True, null=True)
    messagecount = models.IntegerField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbcategory'
        unique_together = (('uuid_field', 'groupid'),)


class Mbdiscussion(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    discussionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    threadid = models.BigIntegerField(unique=True, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbdiscussion'
        unique_together = (('classnameid', 'classpk'), ('uuid_field', 'groupid'),)


class Mbmailinglist(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    mailinglistid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    categoryid = models.BigIntegerField(blank=True, null=True)
    emailaddress = models.CharField(max_length=254, blank=True, null=True)
    inprotocol = models.CharField(max_length=75, blank=True, null=True)
    inservername = models.CharField(max_length=75, blank=True, null=True)
    inserverport = models.IntegerField(blank=True, null=True)
    inusessl = models.BooleanField(blank=True, null=True)
    inusername = models.CharField(max_length=75, blank=True, null=True)
    inpassword = models.CharField(max_length=75, blank=True, null=True)
    inreadinterval = models.IntegerField(blank=True, null=True)
    outemailaddress = models.CharField(max_length=254, blank=True, null=True)
    outcustom = models.BooleanField(blank=True, null=True)
    outservername = models.CharField(max_length=75, blank=True, null=True)
    outserverport = models.IntegerField(blank=True, null=True)
    outusessl = models.BooleanField(blank=True, null=True)
    outusername = models.CharField(max_length=75, blank=True, null=True)
    outpassword = models.CharField(max_length=75, blank=True, null=True)
    allowanonymous = models.BooleanField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'mbmailinglist'
        unique_together = (('groupid', 'categoryid'), ('uuid_field', 'groupid'),)


class Mbmessage(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    messageid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    categoryid = models.BigIntegerField(blank=True, null=True)
    threadid = models.BigIntegerField(blank=True, null=True)
    rootmessageid = models.BigIntegerField(blank=True, null=True)
    parentmessageid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    subject = models.CharField(max_length=75, blank=True, null=True)
    urlsubject = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=75, blank=True, null=True)
    anonymous = models.BooleanField(blank=True, null=True)
    priority = models.FloatField(blank=True, null=True)
    allowpingbacks = models.BooleanField(blank=True, null=True)
    answer = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbmessage'
        unique_together = (('groupid', 'urlsubject'), ('uuid_field', 'groupid'),)


class Mbstatsuser(models.Model):
    statsuserid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    messagecount = models.IntegerField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbstatsuser'
        unique_together = (('groupid', 'userid'),)


class Mbthread(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    threadid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    categoryid = models.BigIntegerField(blank=True, null=True)
    rootmessageid = models.BigIntegerField(blank=True, null=True)
    rootmessageuserid = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=75, blank=True, null=True)
    messagecount = models.IntegerField(blank=True, null=True)
    lastpostbyuserid = models.BigIntegerField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)
    priority = models.FloatField(blank=True, null=True)
    question = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbthread'
        unique_together = (('uuid_field', 'groupid'),)


class Mbthreadflag(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    threadflagid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    threadid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mbthreadflag'
        unique_together = (('userid', 'threadid'), ('uuid_field', 'groupid'),)


class Mdraction(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    actionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    rulegroupinstanceid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=255, blank=True, null=True)  # Field renamed because it ended with '_'.
    typesettings = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mdraction'
        unique_together = (('uuid_field', 'groupid'),)


class Mdrrule(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    ruleid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    rulegroupid = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=255, blank=True, null=True)  # Field renamed because it ended with '_'.
    typesettings = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mdrrule'
        unique_together = (('uuid_field', 'groupid'),)


class Mdrrulegroup(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    rulegroupid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mdrrulegroup'
        unique_together = (('uuid_field', 'groupid'),)


class Mdrrulegroupinstance(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    rulegroupinstanceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    rulegroupid = models.BigIntegerField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mdrrulegroupinstance'
        unique_together = (('classnameid', 'classpk', 'rulegroupid'), ('uuid_field', 'groupid'),)


class Membershiprequest(models.Model):
    mvccversion = models.BigIntegerField()
    membershiprequestid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    replycomments = models.TextField(blank=True, null=True)
    replydate = models.DateTimeField(blank=True, null=True)
    replieruserid = models.BigIntegerField(blank=True, null=True)
    statusid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'membershiprequest'


class Oa2AuthsOa2Scopegrants(models.Model):
    companyid = models.BigIntegerField()
    oauth2authorizationid = models.BigIntegerField(primary_key=True)
    oauth2scopegrantid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'oa2auths_oa2scopegrants'
        unique_together = (('oauth2authorizationid', 'oauth2scopegrantid'),)


class Oauth2Application(models.Model):
    oauth2applicationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    oa2ascopealiasesid = models.BigIntegerField(blank=True, null=True)
    allowedgranttypes = models.CharField(max_length=75, blank=True, null=True)
    clientcredentialuserid = models.BigIntegerField(blank=True, null=True)
    clientcredentialusername = models.CharField(max_length=75, blank=True, null=True)
    clientid = models.CharField(max_length=75, blank=True, null=True)
    clientprofile = models.IntegerField(blank=True, null=True)
    clientsecret = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    features = models.TextField(blank=True, null=True)
    homepageurl = models.TextField(blank=True, null=True)
    iconfileentryid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    privacypolicyurl = models.TextField(blank=True, null=True)
    redirecturis = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth2application'


class Oauth2Applicationscopealiases(models.Model):
    oa2ascopealiasesid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    oauth2applicationid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth2applicationscopealiases'


class Oauth2Authorization(models.Model):
    oauth2authorizationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    oauth2applicationid = models.BigIntegerField(blank=True, null=True)
    oa2ascopealiasesid = models.BigIntegerField(blank=True, null=True)
    accesstokencontent = models.TextField(blank=True, null=True)
    accesstokencontenthash = models.BigIntegerField(blank=True, null=True)
    accesstokencreatedate = models.DateTimeField(blank=True, null=True)
    accesstokenexpirationdate = models.DateTimeField(blank=True, null=True)
    remotehostinfo = models.CharField(max_length=255, blank=True, null=True)
    remoteipinfo = models.CharField(max_length=75, blank=True, null=True)
    refreshtokencontent = models.TextField(blank=True, null=True)
    refreshtokencontenthash = models.BigIntegerField(blank=True, null=True)
    refreshtokencreatedate = models.DateTimeField(blank=True, null=True)
    refreshtokenexpirationdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth2authorization'


class Oauth2Scopegrant(models.Model):
    oauth2scopegrantid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    oa2ascopealiasesid = models.BigIntegerField(blank=True, null=True)
    applicationname = models.CharField(max_length=255, blank=True, null=True)
    bundlesymbolicname = models.CharField(max_length=255, blank=True, null=True)
    scope = models.CharField(max_length=240, blank=True, null=True)
    scopealiases = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth2scopegrant'


class Organization(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    externalreferencecode = models.CharField(max_length=75, blank=True, null=True)
    organizationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentorganizationid = models.BigIntegerField(blank=True, null=True)
    treepath = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    recursable = models.BooleanField(blank=True, null=True)
    regionid = models.BigIntegerField(blank=True, null=True)
    countryid = models.BigIntegerField(blank=True, null=True)
    statusid = models.BigIntegerField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    logoid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'organization_'
        unique_together = (('companyid', 'name'),)


class Orggrouprole(models.Model):
    mvccversion = models.BigIntegerField()
    organizationid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField()
    roleid = models.BigIntegerField()
    companyid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'orggrouprole'
        unique_together = (('organizationid', 'groupid', 'roleid'),)


class Orglabor(models.Model):
    mvccversion = models.BigIntegerField()
    orglaborid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    organizationid = models.BigIntegerField(blank=True, null=True)
    typeid = models.BigIntegerField(blank=True, null=True)
    sunopen = models.IntegerField(blank=True, null=True)
    sunclose = models.IntegerField(blank=True, null=True)
    monopen = models.IntegerField(blank=True, null=True)
    monclose = models.IntegerField(blank=True, null=True)
    tueopen = models.IntegerField(blank=True, null=True)
    tueclose = models.IntegerField(blank=True, null=True)
    wedopen = models.IntegerField(blank=True, null=True)
    wedclose = models.IntegerField(blank=True, null=True)
    thuopen = models.IntegerField(blank=True, null=True)
    thuclose = models.IntegerField(blank=True, null=True)
    friopen = models.IntegerField(blank=True, null=True)
    friclose = models.IntegerField(blank=True, null=True)
    satopen = models.IntegerField(blank=True, null=True)
    satclose = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'orglabor'


class Passwordpolicy(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    passwordpolicyid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    defaultpolicy = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    changeable = models.BooleanField(blank=True, null=True)
    changerequired = models.BooleanField(blank=True, null=True)
    minage = models.BigIntegerField(blank=True, null=True)
    checksyntax = models.BooleanField(blank=True, null=True)
    allowdictionarywords = models.BooleanField(blank=True, null=True)
    minalphanumeric = models.IntegerField(blank=True, null=True)
    minlength = models.IntegerField(blank=True, null=True)
    minlowercase = models.IntegerField(blank=True, null=True)
    minnumbers = models.IntegerField(blank=True, null=True)
    minsymbols = models.IntegerField(blank=True, null=True)
    minuppercase = models.IntegerField(blank=True, null=True)
    regex = models.TextField(blank=True, null=True)
    history = models.BooleanField(blank=True, null=True)
    historycount = models.IntegerField(blank=True, null=True)
    expireable = models.BooleanField(blank=True, null=True)
    maxage = models.BigIntegerField(blank=True, null=True)
    warningtime = models.BigIntegerField(blank=True, null=True)
    gracelimit = models.IntegerField(blank=True, null=True)
    lockout = models.BooleanField(blank=True, null=True)
    maxfailure = models.IntegerField(blank=True, null=True)
    lockoutduration = models.BigIntegerField(blank=True, null=True)
    requireunlock = models.BooleanField(blank=True, null=True)
    resetfailurecount = models.BigIntegerField(blank=True, null=True)
    resetticketmaxage = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'passwordpolicy'
        unique_together = (('companyid', 'name'),)


class Passwordpolicyrel(models.Model):
    mvccversion = models.BigIntegerField()
    passwordpolicyrelid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    passwordpolicyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'passwordpolicyrel'
        unique_together = (('classnameid', 'classpk'),)


class Passwordtracker(models.Model):
    mvccversion = models.BigIntegerField()
    passwordtrackerid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    password_field = models.CharField(db_column='password_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'passwordtracker'


class Phone(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    phoneid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    number_field = models.CharField(db_column='number_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    extension = models.CharField(max_length=75, blank=True, null=True)
    typeid = models.BigIntegerField(blank=True, null=True)
    primary_field = models.BooleanField(db_column='primary_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'phone'


class Pluginsetting(models.Model):
    mvccversion = models.BigIntegerField()
    pluginsettingid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    pluginid = models.CharField(max_length=75, blank=True, null=True)
    plugintype = models.CharField(max_length=75, blank=True, null=True)
    roles = models.TextField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'pluginsetting'
        unique_together = (('companyid', 'pluginid', 'plugintype'),)


class Pollschoice(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    choiceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    questionid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pollschoice'
        unique_together = (('uuid_field', 'groupid'), ('questionid', 'name'),)


class Pollsquestion(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    questionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    lastvotedate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pollsquestion'
        unique_together = (('uuid_field', 'groupid'),)


class Pollsvote(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    voteid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    questionid = models.BigIntegerField(blank=True, null=True)
    choiceid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    votedate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pollsvote'
        unique_together = (('uuid_field', 'groupid'),)


class Portalpreferences(models.Model):
    mvccversion = models.BigIntegerField()
    portalpreferencesid = models.BigIntegerField(primary_key=True)
    ownerid = models.BigIntegerField(blank=True, null=True)
    ownertype = models.IntegerField(blank=True, null=True)
    preferences = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'portalpreferences'


class Portlet(models.Model):
    mvccversion = models.BigIntegerField()
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    companyid = models.BigIntegerField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    roles = models.TextField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'portlet'
        unique_together = (('companyid', 'portletid'),)


class Portletitem(models.Model):
    mvccversion = models.BigIntegerField()
    portletitemid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'portletitem'


class Portletpreferences(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    portletpreferencesid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    ownerid = models.BigIntegerField(blank=True, null=True)
    ownertype = models.IntegerField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    preferences = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'portletpreferences'
        unique_together = (('portletpreferencesid', 'ctcollectionid'), ('ownerid', 'ownertype', 'plid', 'portletid', 'ctcollectionid'),)


class QuartzBlobTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    blob_data = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_blob_triggers'
        unique_together = (('sched_name', 'trigger_name', 'trigger_group'),)


class QuartzCalendars(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    calendar_name = models.CharField(max_length=200)
    calendar = models.BinaryField()

    class Meta:
        managed = False
        db_table = 'quartz_calendars'
        unique_together = (('sched_name', 'calendar_name'),)


class QuartzCronTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    cron_expression = models.CharField(max_length=200)
    time_zone_id = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_cron_triggers'
        unique_together = (('sched_name', 'trigger_name', 'trigger_group'),)


class QuartzFiredTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    entry_id = models.CharField(max_length=95)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    instance_name = models.CharField(max_length=200)
    fired_time = models.BigIntegerField()
    priority = models.IntegerField()
    state = models.CharField(max_length=16)
    job_name = models.CharField(max_length=200, blank=True, null=True)
    job_group = models.CharField(max_length=200, blank=True, null=True)
    is_nonconcurrent = models.BooleanField(blank=True, null=True)
    requests_recovery = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_fired_triggers'
        unique_together = (('sched_name', 'entry_id'),)


class QuartzJobDetails(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    job_name = models.CharField(max_length=200)
    job_group = models.CharField(max_length=200)
    description = models.CharField(max_length=250, blank=True, null=True)
    job_class_name = models.CharField(max_length=250)
    is_durable = models.BooleanField()
    is_nonconcurrent = models.BooleanField()
    is_update_data = models.BooleanField()
    requests_recovery = models.BooleanField()
    job_data = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_job_details'
        unique_together = (('sched_name', 'job_name', 'job_group'),)


class QuartzLocks(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    lock_name = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'quartz_locks'
        unique_together = (('sched_name', 'lock_name'),)


class QuartzPausedTriggerGrps(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_group = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'quartz_paused_trigger_grps'
        unique_together = (('sched_name', 'trigger_group'),)


class QuartzSchedulerState(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    instance_name = models.CharField(max_length=200)
    last_checkin_time = models.BigIntegerField()
    checkin_interval = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'quartz_scheduler_state'
        unique_together = (('sched_name', 'instance_name'),)


class QuartzSimpleTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    repeat_count = models.BigIntegerField()
    repeat_interval = models.BigIntegerField()
    times_triggered = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'quartz_simple_triggers'
        unique_together = (('sched_name', 'trigger_name', 'trigger_group'),)


class QuartzSimpropTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    str_prop_1 = models.CharField(max_length=512, blank=True, null=True)
    str_prop_2 = models.CharField(max_length=512, blank=True, null=True)
    str_prop_3 = models.CharField(max_length=512, blank=True, null=True)
    int_prop_1 = models.IntegerField(blank=True, null=True)
    int_prop_2 = models.IntegerField(blank=True, null=True)
    long_prop_1 = models.BigIntegerField(blank=True, null=True)
    long_prop_2 = models.BigIntegerField(blank=True, null=True)
    dec_prop_1 = models.DecimalField(max_digits=13, decimal_places=4, blank=True, null=True)
    dec_prop_2 = models.DecimalField(max_digits=13, decimal_places=4, blank=True, null=True)
    bool_prop_1 = models.BooleanField(blank=True, null=True)
    bool_prop_2 = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_simprop_triggers'
        unique_together = (('sched_name', 'trigger_name', 'trigger_group'),)


class QuartzTriggers(models.Model):
    sched_name = models.CharField(primary_key=True, max_length=120)
    trigger_name = models.CharField(max_length=200)
    trigger_group = models.CharField(max_length=200)
    job_name = models.CharField(max_length=200)
    job_group = models.CharField(max_length=200)
    description = models.CharField(max_length=250, blank=True, null=True)
    next_fire_time = models.BigIntegerField(blank=True, null=True)
    prev_fire_time = models.BigIntegerField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    trigger_state = models.CharField(max_length=16)
    trigger_type = models.CharField(max_length=8)
    start_time = models.BigIntegerField()
    end_time = models.BigIntegerField(blank=True, null=True)
    calendar_name = models.CharField(max_length=200, blank=True, null=True)
    misfire_instr = models.IntegerField(blank=True, null=True)
    job_data = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'quartz_triggers'
        unique_together = (('sched_name', 'trigger_name', 'trigger_group'),)


class Ratingsentry(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    entryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    score = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ratingsentry'
        unique_together = (('userid', 'classnameid', 'classpk'),)


class Ratingsstats(models.Model):
    statsid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    totalentries = models.IntegerField(blank=True, null=True)
    totalscore = models.FloatField(blank=True, null=True)
    averagescore = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ratingsstats'
        unique_together = (('classnameid', 'classpk'),)


class Readingtimeentry(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    readingtimeentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    readingtime = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'readingtimeentry'
        unique_together = (('groupid', 'classnameid', 'classpk'), ('uuid_field', 'groupid'),)


class Recentlayoutbranch(models.Model):
    mvccversion = models.BigIntegerField()
    recentlayoutbranchid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    layoutbranchid = models.BigIntegerField(blank=True, null=True)
    layoutsetbranchid = models.BigIntegerField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recentlayoutbranch'
        unique_together = (('userid', 'layoutsetbranchid', 'plid'),)


class Recentlayoutrevision(models.Model):
    mvccversion = models.BigIntegerField()
    recentlayoutrevisionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    layoutrevisionid = models.BigIntegerField(blank=True, null=True)
    layoutsetbranchid = models.BigIntegerField(blank=True, null=True)
    plid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recentlayoutrevision'
        unique_together = (('userid', 'layoutsetbranchid', 'plid'),)


class Recentlayoutsetbranch(models.Model):
    mvccversion = models.BigIntegerField()
    recentlayoutsetbranchid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    layoutsetbranchid = models.BigIntegerField(blank=True, null=True)
    layoutsetid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recentlayoutsetbranch'
        unique_together = (('userid', 'layoutsetid'),)


class Redirectentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    redirectentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    destinationurl = models.TextField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)
    lastoccurrencedate = models.DateTimeField(blank=True, null=True)
    permanent_field = models.BooleanField(db_column='permanent_', blank=True, null=True)  # Field renamed because it ended with '_'.
    sourceurl = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'redirectentry'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'sourceurl'),)


class Redirectnotfoundentry(models.Model):
    mvccversion = models.BigIntegerField()
    redirectnotfoundentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    hits = models.BigIntegerField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'redirectnotfoundentry'
        unique_together = (('groupid', 'url'),)


class Region(models.Model):
    mvccversion = models.BigIntegerField()
    regionid = models.BigIntegerField(primary_key=True)
    countryid = models.BigIntegerField(blank=True, null=True)
    regioncode = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'region'
        unique_together = (('countryid', 'regioncode'),)


class Release(models.Model):
    mvccversion = models.BigIntegerField()
    releaseid = models.BigIntegerField(primary_key=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    servletcontextname = models.CharField(unique=True, max_length=75, blank=True, null=True)
    schemaversion = models.CharField(max_length=75, blank=True, null=True)
    buildnumber = models.IntegerField(blank=True, null=True)
    builddate = models.DateTimeField(blank=True, null=True)
    verified = models.BooleanField(blank=True, null=True)
    state_field = models.IntegerField(db_column='state_', blank=True, null=True)  # Field renamed because it ended with '_'.
    teststring = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'release_'


class Repository(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    repositoryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    dlfolderid = models.BigIntegerField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'repository'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'name', 'portletid'),)


class Repositoryentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    repositoryentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    repositoryid = models.BigIntegerField(blank=True, null=True)
    mappedid = models.CharField(max_length=255, blank=True, null=True)
    manualcheckinrequired = models.BooleanField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'repositoryentry'
        unique_together = (('uuid_field', 'groupid'), ('repositoryid', 'mappedid'),)


class Resourceaction(models.Model):
    mvccversion = models.BigIntegerField()
    resourceactionid = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    actionid = models.CharField(max_length=75, blank=True, null=True)
    bitwisevalue = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resourceaction'
        unique_together = (('name', 'actionid'),)


class Resourcepermission(models.Model):
    mvccversion = models.BigIntegerField()
    ctcollectionid = models.BigIntegerField()
    resourcepermissionid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    scope = models.IntegerField(blank=True, null=True)
    primkey = models.CharField(max_length=255, blank=True, null=True)
    primkeyid = models.BigIntegerField(blank=True, null=True)
    roleid = models.BigIntegerField(blank=True, null=True)
    ownerid = models.BigIntegerField(blank=True, null=True)
    actionids = models.BigIntegerField(blank=True, null=True)
    viewactionid = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resourcepermission'
        unique_together = (('resourcepermissionid', 'ctcollectionid'), ('companyid', 'name', 'scope', 'primkey', 'roleid', 'ctcollectionid'),)


class RiskRisk(models.Model):
    id = models.BigAutoField(primary_key=True)
    holdername = models.CharField(db_column='holderName', max_length=255)  # Field name made lowercase.
    amount = models.CharField(max_length=255)
    outstandingyears = models.CharField(db_column='outstandingYears', max_length=255)  # Field name made lowercase.
    creatiaondate = models.CharField(db_column='creatiaonDate', max_length=255)  # Field name made lowercase.
    modificationdate = models.CharField(db_column='modificationDate', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'risk_risk'


class Role(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    roleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    subtype = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'role_'
        unique_together = (('companyid', 'classnameid', 'classpk'), ('companyid', 'name'),)


class Sapentry(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    sapentryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    allowedservicesignatures = models.TextField(blank=True, null=True)
    defaultsapentry = models.BooleanField(blank=True, null=True)
    enabled = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    title = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sapentry'


class Segmentsentry(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    segmentsentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsentrykey = models.CharField(max_length=75, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.
    criteria = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsentry'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'segmentsentrykey'),)


class Segmentsentryrel(models.Model):
    mvccversion = models.BigIntegerField()
    segmentsentryrelid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsentryrel'
        unique_together = (('segmentsentryid', 'classnameid', 'classpk'),)


class Segmentsentryrole(models.Model):
    mvccversion = models.BigIntegerField()
    segmentsentryroleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    roleid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsentryrole'
        unique_together = (('segmentsentryid', 'roleid'),)


class Segmentsexperience(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    segmentsexperienceid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    segmentsexperiencekey = models.CharField(max_length=75, blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsexperience'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'classnameid', 'classpk', 'priority'), ('groupid', 'segmentsexperiencekey'),)


class Segmentsexperiment(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    segmentsexperimentid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsentryid = models.BigIntegerField(blank=True, null=True)
    segmentsexperienceid = models.BigIntegerField(blank=True, null=True)
    segmentsexperimentkey = models.CharField(max_length=75, blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsexperiment'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'segmentsexperimentkey'),)


class Segmentsexperimentrel(models.Model):
    mvccversion = models.BigIntegerField()
    segmentsexperimentrelid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    segmentsexperimentid = models.BigIntegerField(blank=True, null=True)
    segmentsexperienceid = models.BigIntegerField(blank=True, null=True)
    split = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'segmentsexperimentrel'
        unique_together = (('segmentsexperimentid', 'segmentsexperienceid'),)


class Servicecomponent(models.Model):
    mvccversion = models.BigIntegerField()
    servicecomponentid = models.BigIntegerField(primary_key=True)
    buildnamespace = models.CharField(max_length=75, blank=True, null=True)
    buildnumber = models.BigIntegerField(blank=True, null=True)
    builddate = models.BigIntegerField(blank=True, null=True)
    data_field = models.TextField(db_column='data_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'servicecomponent'
        unique_together = (('buildnamespace', 'buildnumber'),)


class Sharingentry(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    sharingentryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    touserid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    shareable = models.BooleanField(blank=True, null=True)
    actionids = models.BigIntegerField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sharingentry'
        unique_together = (('touserid', 'classnameid', 'classpk'), ('uuid_field', 'groupid'),)


class Sitefriendlyurl(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    sitefriendlyurlid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    friendlyurl = models.CharField(max_length=75, blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sitefriendlyurl'
        unique_together = (('companyid', 'groupid', 'languageid'), ('uuid_field', 'groupid'), ('companyid', 'friendlyurl'),)


class Sitenavigationmenu(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    sitenavigationmenuid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    auto_field = models.BooleanField(db_column='auto_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sitenavigationmenu'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'name'),)


class Sitenavigationmenuitem(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    sitenavigationmenuitemid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    sitenavigationmenuid = models.BigIntegerField(blank=True, null=True)
    parentsitenavigationmenuitemid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    typesettings = models.TextField(blank=True, null=True)
    order_field = models.IntegerField(db_column='order_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sitenavigationmenuitem'
        unique_together = (('uuid_field', 'groupid'),)


class Socialactivity(models.Model):
    activityid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.BigIntegerField(blank=True, null=True)
    activitysetid = models.BigIntegerField(blank=True, null=True)
    mirroractivityid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    parentclassnameid = models.BigIntegerField(blank=True, null=True)
    parentclasspk = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    extradata = models.TextField(blank=True, null=True)
    receiveruserid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialactivity'
        unique_together = (('groupid', 'userid', 'createdate', 'classnameid', 'classpk', 'type_field', 'receiveruserid'),)


class Socialactivityachievement(models.Model):
    activityachievementid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    firstingroup = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialactivityachievement'
        unique_together = (('groupid', 'userid', 'name'),)


class Socialactivitycounter(models.Model):
    activitycounterid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    ownertype = models.IntegerField(blank=True, null=True)
    currentvalue = models.IntegerField(blank=True, null=True)
    totalvalue = models.IntegerField(blank=True, null=True)
    gracevalue = models.IntegerField(blank=True, null=True)
    startperiod = models.IntegerField(blank=True, null=True)
    endperiod = models.IntegerField(blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'socialactivitycounter'
        unique_together = (('groupid', 'classnameid', 'classpk', 'name', 'ownertype', 'endperiod'), ('groupid', 'classnameid', 'classpk', 'name', 'ownertype', 'startperiod'),)


class Socialactivitylimit(models.Model):
    activitylimitid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    activitytype = models.IntegerField(blank=True, null=True)
    activitycountername = models.CharField(max_length=75, blank=True, null=True)
    value = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialactivitylimit'
        unique_together = (('groupid', 'userid', 'classnameid', 'classpk', 'activitytype', 'activitycountername'),)


class Socialactivityset(models.Model):
    activitysetid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.BigIntegerField(blank=True, null=True)
    modifieddate = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    extradata = models.TextField(blank=True, null=True)
    activitycount = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialactivityset'


class Socialactivitysetting(models.Model):
    activitysettingid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    activitytype = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    value = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialactivitysetting'


class Socialrelation(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    relationid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.BigIntegerField(blank=True, null=True)
    userid1 = models.BigIntegerField(blank=True, null=True)
    userid2 = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'socialrelation'
        unique_together = (('userid1', 'userid2', 'type_field'),)


class Socialrequest(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    requestid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    createdate = models.BigIntegerField(blank=True, null=True)
    modifieddate = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    extradata = models.TextField(blank=True, null=True)
    receiveruserid = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socialrequest'
        unique_together = (('userid', 'classnameid', 'classpk', 'type_field', 'receiveruserid'), ('uuid_field', 'groupid'),)


class Subscription(models.Model):
    mvccversion = models.BigIntegerField()
    subscriptionid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    frequency = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subscription'
        unique_together = (('companyid', 'userid', 'classnameid', 'classpk'),)


class Systemevent(models.Model):
    mvccversion = models.BigIntegerField()
    systemeventid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    classuuid = models.CharField(max_length=75, blank=True, null=True)
    referrerclassnameid = models.BigIntegerField(blank=True, null=True)
    parentsystemeventid = models.BigIntegerField(blank=True, null=True)
    systemeventsetkey = models.BigIntegerField(blank=True, null=True)
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    extradata = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'systemevent'


class Team(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    teamid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'team'
        unique_together = (('groupid', 'name'), ('uuid_field', 'groupid'),)


class Ticket(models.Model):
    mvccversion = models.BigIntegerField()
    ticketid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    key_field = models.CharField(db_column='key_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    type_field = models.IntegerField(db_column='type_', blank=True, null=True)  # Field renamed because it ended with '_'.
    extrainfo = models.TextField(blank=True, null=True)
    expirationdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ticket'


class Trashentry(models.Model):
    mvccversion = models.BigIntegerField()
    entryid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    systemeventsetkey = models.BigIntegerField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trashentry'
        unique_together = (('classnameid', 'classpk'),)


class Trashversion(models.Model):
    mvccversion = models.BigIntegerField()
    versionid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    entryid = models.BigIntegerField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    typesettings = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trashversion'
        unique_together = (('classnameid', 'classpk'),)


class UplaudsBatchDetail(models.Model):
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    batch = models.BigIntegerField(blank=True, null=True)
    batchtype = models.CharField(max_length=75, blank=True, null=True)
    processed = models.IntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_batch_detail'


class UplaudsCorpBankcharges(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corpbankchargesid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    srn = models.CharField(max_length=75, blank=True, null=True)
    chargeid = models.CharField(max_length=75, blank=True, null=True)
    chargeholdername = models.CharField(max_length=250, blank=True, null=True)
    chrcreationdate = models.DateTimeField(blank=True, null=True)
    chrmodificatondate = models.DateTimeField(blank=True, null=True)
    chrsatisfactiondare = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    address = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_bankcharges'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpCreditInstrument(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    credit_instrument_id = models.BigIntegerField(primary_key=True)
    credit_rating_id = models.BigIntegerField(blank=True, null=True)
    instrument = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_credit_instrument'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpFinValues(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    fy_id = models.BigIntegerField(primary_key=True)
    cf_id = models.CharField(max_length=75, blank=True, null=True)
    year = models.CharField(max_length=75, blank=True, null=True)
    values_field = models.CharField(db_column='values_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_fin_values'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpFinancial(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    cf_id = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    year = models.CharField(max_length=75, blank=True, null=True)
    sapa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    repss = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toreop = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cmcde = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cifw = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pstr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tocs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    grpr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    indr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    fefu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    misin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonoi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    siaob = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ctpob = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    swe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    gratu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    oebe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toebe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    itwcl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inol = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    finch = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otbc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tofc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    deam = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bede = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    once = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    mase = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bipa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tome = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    rent = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ratx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    remc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inrrc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prst = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prflcc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pwfu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reaaf = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    acch = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trco = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    comex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ofex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    misex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    togae = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tooe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    exitm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otnre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    difre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inod = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tosdd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbtx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cutx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    totx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    npatx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ebitda = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ebit = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    adjpt = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    epa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cseq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trrev = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toinv = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cuinv = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tnas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lsade = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    totas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lsam = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toinas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tofa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    noci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotlad = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftxas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otnoca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tooas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trpa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtbr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtrp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otcl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tocl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotbr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lofrep = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    nocd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    detxl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otncl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tool = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toli = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    resup = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prfst = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toeq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tole = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoinc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfodea = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfobd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfonce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfar = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfstla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfap = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfnpstd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfstp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfdta = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfocl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftoa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfice = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfinci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfltla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoicfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftia = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfltd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfps = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftcdp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfcs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfofcfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftfa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ccf = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    becb = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ecb = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bpm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbtm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    netpm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reonwe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reoce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reoa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tode = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    atr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    crre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    qure = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    deeqr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotder = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    capm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    eqmlp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cuat = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    wocr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    woctr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    incr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    descre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    grpm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    emcos = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    mrcs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slgr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prgr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slgtprg = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slcan = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prcagr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    csrfs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cspfp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dasou = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dainvo = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dapaou = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cscocy = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    csrexp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    direxp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    otexp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    adjment = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reaveq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reacem = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    aatr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cara = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    oeq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    raa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_financial'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpFinancialTest(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    cf_id = models.BigIntegerField()
    cin = models.CharField(max_length=75, blank=True, null=True)
    year = models.CharField(max_length=75, blank=True, null=True)
    sapa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    repss = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toreop = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cmcde = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cifw = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pstr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tocs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    grpr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    indr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    fefu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    misin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonoi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toin = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    siaob = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ctpob = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    swe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    gratu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    oebe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toebe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    itwcl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inol = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    finch = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otbc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tofc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    deam = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bede = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    once = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    mase = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bipa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tome = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    rent = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ratx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    remc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inrrc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prst = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prflcc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pwfu = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reaaf = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    acch = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trco = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    comex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ofex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    misex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    togae = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tooe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    exitm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otnre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tonre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    difre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inod = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tosdd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toex = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbtx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cutx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    totx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    npatx = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ebitda = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ebit = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    adjpt = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    epa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cseq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trrev = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toinv = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cuinv = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tnas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lsade = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    totas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    inas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lsam = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toinas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tofa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    noci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotlad = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftxas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otnoca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tooas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toas = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    trpa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtbr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shtrp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otcl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tocl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotbr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lofrep = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    nocd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    detxl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    otncl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tool = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toli = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    shca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    resup = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prfst = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    toeq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tole = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoinc = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfodea = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfobd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfonce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfar = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoca = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfstla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfap = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfnpstd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfstp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfdta = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dftl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfocl = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftoa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfice = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfinci = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfltla = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfoicfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftia = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfltd = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfps = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftcdp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfcs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cfofcfi = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cftfa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ccf = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    becb = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    ecb = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    bpm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    pbtm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    netpm_p = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reonwe = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reoce = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reoa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    tode = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    atr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    crre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    qure = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    deeqr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    lotder = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    capm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    eqmlp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cuat = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    wocr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    woctr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    incr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    descre = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    grpm = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    emcos = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    mrcs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slgr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prgr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slgtprg = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    slcan = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    prcagr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    csrfs = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cspfp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dasou = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dainvo = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    dapaou = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cscocy = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    csrexp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    direxp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    otexp = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    adjment = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reaveq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    reacem = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    aatr = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    cara = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    oeq = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    raa = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_financial_test'


class UplaudsCorpGst(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    ctax_id = models.CharField(max_length=75, blank=True, null=True)
    result = models.CharField(max_length=75, blank=True, null=True)
    financial_year = models.CharField(max_length=75, blank=True, null=True)
    tex_period = models.CharField(max_length=75, blank=True, null=True)
    filing_date = models.CharField(max_length=75, blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_gst'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpGstReturn(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corpgstreturnid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    gst = models.CharField(max_length=75, blank=True, null=True)
    textype = models.CharField(max_length=75, blank=True, null=True)
    returntype = models.CharField(max_length=75, blank=True, null=True)
    year = models.BigIntegerField(blank=True, null=True)
    month = models.BigIntegerField(blank=True, null=True)
    datefilling = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_gst_return'


class UplaudsCorpPf(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corppfid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    pfno = models.CharField(max_length=75, blank=True, null=True)
    trrn = models.CharField(max_length=75, blank=True, null=True)
    dateofcredit = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    wagemonth = models.DateTimeField(blank=True, null=True)
    noofemployee = models.BigIntegerField(blank=True, null=True)
    ecr = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_pf'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpRoc(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corprocid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    formname = models.CharField(max_length=75, blank=True, null=True)
    year = models.BigIntegerField(blank=True, null=True)
    date_field = models.DateTimeField(db_column='date_', blank=True, null=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_roc'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpSearchHistory(models.Model):
    id_field = models.CharField(db_column='id_', primary_key=True, max_length=75)  # Field renamed because it ended with '_'.
    cin = models.CharField(max_length=75, blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    active_field = models.BooleanField(db_column='active_', blank=True, null=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_search_history'


class UplaudsCorpShareholding(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corpshareholdingid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    holdingdate = models.DateTimeField(blank=True, null=True)
    shareholdername = models.CharField(max_length=75, blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    numbershare = models.BigIntegerField(blank=True, null=True)
    facevalue = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_shareholding'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpTaxIds(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    ctax_id = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    gst = models.CharField(max_length=75, blank=True, null=True)
    pan = models.CharField(max_length=75, blank=True, null=True)
    state_field = models.CharField(db_column='state_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_tax_ids'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorpTds(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corptdsid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    tanno = models.CharField(max_length=75, blank=True, null=True)
    challantenderdate = models.CharField(max_length=75, blank=True, null=True)
    challanserialno = models.BigIntegerField(blank=True, null=True)
    receiveddate = models.CharField(max_length=75, blank=True, null=True)
    majorheadcode = models.CharField(max_length=75, blank=True, null=True)
    minorheadcode = models.CharField(max_length=75, blank=True, null=True)
    natureofpayment = models.CharField(max_length=75, blank=True, null=True)
    amount = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_tds'


class UplaudsCorpTrademark(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    corptrademarkid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    trademarkname = models.CharField(max_length=75, blank=True, null=True)
    trdclass = models.CharField(max_length=75, blank=True, null=True)
    applicationdate = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    gudservicesdsc = models.CharField(max_length=75, blank=True, null=True)
    applicantaddress = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corp_trademark'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorporation(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    cin = models.CharField(primary_key=True, max_length=75)
    reference_id = models.BigIntegerField(unique=True, blank=True, null=True)
    name = models.CharField(max_length=1000, blank=True, null=True)
    corporation_class = models.CharField(max_length=75, blank=True, null=True)
    category = models.CharField(max_length=75, blank=True, null=True)
    sub_category = models.CharField(max_length=75, blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    registration_date = models.DateTimeField(blank=True, null=True)
    authorized_capital = models.FloatField(blank=True, null=True)
    paidup_capital = models.FloatField(blank=True, null=True)
    activity_code = models.CharField(max_length=75, blank=True, null=True)
    business_desc = models.TextField(blank=True, null=True)
    industry = models.CharField(max_length=75, blank=True, null=True)
    sector = models.CharField(max_length=75, blank=True, null=True)
    classifications = models.TextField(blank=True, null=True)
    transaction_status = models.CharField(max_length=75, blank=True, null=True)
    year_founded = models.CharField(max_length=75, blank=True, null=True)
    registered_state = models.CharField(max_length=75, blank=True, null=True)
    city = models.CharField(max_length=75, blank=True, null=True)
    country = models.CharField(max_length=75, blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    obligation_of_contribution = models.CharField(max_length=75, blank=True, null=True)
    number_of_designated_partners = models.CharField(max_length=75, blank=True, null=True)
    number_of_partners = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    emailid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corporation'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCorporationTest(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    cin = models.CharField(max_length=75)
    reference_id = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=1000, blank=True, null=True)
    corporation_class = models.CharField(max_length=75, blank=True, null=True)
    category = models.CharField(max_length=75, blank=True, null=True)
    sub_category = models.CharField(max_length=75, blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    registration_date = models.DateTimeField(blank=True, null=True)
    authorized_capital = models.FloatField(blank=True, null=True)
    paidup_capital = models.FloatField(blank=True, null=True)
    activity_code = models.CharField(max_length=75, blank=True, null=True)
    business_desc = models.TextField(blank=True, null=True)
    industry = models.CharField(max_length=75, blank=True, null=True)
    sector = models.CharField(max_length=75, blank=True, null=True)
    classifications = models.TextField(blank=True, null=True)
    transaction_status = models.CharField(max_length=75, blank=True, null=True)
    year_founded = models.CharField(max_length=75, blank=True, null=True)
    registered_state = models.CharField(max_length=75, blank=True, null=True)
    city = models.CharField(max_length=75, blank=True, null=True)
    country = models.CharField(max_length=75, blank=True, null=True)
    office_address = models.TextField(blank=True, null=True)
    obligation_of_contribution = models.CharField(max_length=75, blank=True, null=True)
    number_of_designated_partners = models.CharField(max_length=75, blank=True, null=True)
    number_of_partners = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_corporation_test'


class UplaudsCreditRating(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    credit_rating_id = models.BigIntegerField(primary_key=True)
    source_id = models.BigIntegerField(blank=True, null=True)
    instrument_category = models.CharField(max_length=75, blank=True, null=True)
    rating = models.CharField(max_length=75, blank=True, null=True)
    review_date = models.DateTimeField(blank=True, null=True)
    report_url = models.DateTimeField(blank=True, null=True)
    outlook = models.DateTimeField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_credit_rating'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCreditSource(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    source_id = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    source = models.CharField(max_length=75, blank=True, null=True)
    source_corp_ld = models.BigIntegerField(blank=True, null=True)
    source_corp_name = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_credit_source'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsCrisilCompanyDetails(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    crisildetailsid = models.BigIntegerField(primary_key=True)
    instruments = models.CharField(max_length=250, blank=True, null=True)
    detdate = models.CharField(max_length=75, blank=True, null=True)
    instrumentcategory = models.CharField(max_length=400, blank=True, null=True)
    ispastrating = models.BooleanField(blank=True, null=True)
    companyname = models.CharField(max_length=250, blank=True, null=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    compdescription = models.CharField(max_length=300, blank=True, null=True)
    createddate = models.DateTimeField(blank=True, null=True)
    creditid = models.CharField(max_length=75, blank=True, null=True)
    outlook = models.CharField(max_length=250, blank=True, null=True)
    rating = models.CharField(max_length=250, blank=True, null=True)
    reportdescription = models.CharField(max_length=450, blank=True, null=True)
    reporturl = models.CharField(max_length=450, blank=True, null=True)
    sourcetype = models.CharField(max_length=250, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_crisil_company_details'


class UplaudsDirector(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    din = models.CharField(primary_key=True, max_length=75)
    name = models.CharField(max_length=75, blank=True, null=True)
    haspan = models.CharField(max_length=75, blank=True, null=True)
    din_deactivated = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_director'
        unique_together = (('uuid_field', 'groupid'),)


class UplaudsDirectorCorporation(models.Model):
    id_field = models.BigIntegerField(db_column='id_', primary_key=True)  # Field renamed because it ended with '_'.
    din = models.CharField(max_length=75, blank=True, null=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    designation = models.CharField(max_length=75, blank=True, null=True)
    appointment_date = models.DateTimeField(blank=True, null=True)
    charges = models.FloatField(blank=True, null=True)
    deactivated = models.CharField(max_length=75, blank=True, null=True)
    disqualified = models.CharField(max_length=75, blank=True, null=True)
    capitalization = models.FloatField(blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_director_corporation'


class UplaudsFavouriteCompany(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    favcompayid = models.CharField(primary_key=True, max_length=75)
    userid = models.BigIntegerField(blank=True, null=True)
    favcompayname = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateField(blank=True, null=True)
    modifieddate = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_favourite_company'


class UplaudsGstCompany(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    gstcompanyid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    companyname = models.CharField(max_length=250, blank=True, null=True)
    gstin = models.CharField(max_length=75, blank=True, null=True)
    legalnameofbussiness = models.CharField(max_length=250, blank=True, null=True)
    tradename = models.CharField(max_length=250, blank=True, null=True)
    gstdetaildate = models.CharField(max_length=250, blank=True, null=True)
    administrativeoffice = models.CharField(max_length=250, blank=True, null=True)
    otheroffice = models.CharField(max_length=250, blank=True, null=True)
    principleplaceofbussiness = models.CharField(max_length=250, blank=True, null=True)
    panno = models.CharField(max_length=250, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_gst_company'


class UplaudsGstFiling(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    gstfilingid = models.BigIntegerField(primary_key=True)
    gstin = models.CharField(max_length=75, blank=True, null=True)
    gsttype = models.CharField(max_length=250, blank=True, null=True)
    financialyear = models.CharField(max_length=75, blank=True, null=True)
    taxperiod = models.CharField(max_length=75, blank=True, null=True)
    dateoffiling = models.CharField(max_length=75, blank=True, null=True)
    status = models.CharField(max_length=250, blank=True, null=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_gst_filing'


class UplaudsGstGoodsAndServices(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    gstgoodsandservicesid = models.BigIntegerField(primary_key=True)
    gstin = models.CharField(max_length=75, blank=True, null=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    gstype = models.CharField(max_length=250, blank=True, null=True)
    hsn = models.CharField(max_length=75, blank=True, null=True)
    description = models.CharField(max_length=1500, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_gst_goods_and_services'


class UplaudsIcra(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    icraid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    companyname = models.CharField(max_length=250, blank=True, null=True)
    sectorname = models.CharField(max_length=250, blank=True, null=True)
    longterm = models.CharField(max_length=250, blank=True, null=True)
    shorterm = models.CharField(max_length=250, blank=True, null=True)
    mediumterm = models.CharField(max_length=250, blank=True, null=True)
    issuer = models.CharField(max_length=250, blank=True, null=True)
    outlook = models.CharField(max_length=250, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_icra'


class UplaudsIcraDetails(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    icradetailsid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    companyname = models.CharField(max_length=250, blank=True, null=True)
    detaildate = models.CharField(max_length=75, blank=True, null=True)
    link = models.CharField(max_length=250, blank=True, null=True)
    title = models.CharField(max_length=250, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_icra_details'


class UplaudsPfDetails(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    pfdetailsid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    companyname = models.CharField(max_length=250, blank=True, null=True)
    establishmentid = models.CharField(max_length=250, blank=True, null=True)
    establishmentname = models.CharField(max_length=250, blank=True, null=True)
    officename = models.CharField(max_length=250, blank=True, null=True)
    establishmentstatus = models.CharField(max_length=250, blank=True, null=True)
    postcoveragestatus = models.CharField(max_length=250, blank=True, null=True)
    exemptionstatus = models.CharField(max_length=250, blank=True, null=True)
    workingstatus = models.CharField(max_length=250, blank=True, null=True)
    coveragesection = models.CharField(max_length=250, blank=True, null=True)
    actionablestatus = models.CharField(max_length=250, blank=True, null=True)
    dateofcoverage = models.CharField(max_length=250, blank=True, null=True)
    nameofestablishmentasperpan = models.CharField(max_length=250, blank=True, null=True)
    registrationstatusonecrportal = models.CharField(max_length=250, blank=True, null=True)
    panstatus = models.CharField(max_length=250, blank=True, null=True)
    sectionapplicable = models.CharField(max_length=250, blank=True, null=True)
    primarybusinessactivity = models.CharField(max_length=250, blank=True, null=True)
    esiccode = models.CharField(max_length=250, blank=True, null=True)
    ownershiptype = models.CharField(max_length=250, blank=True, null=True)
    dateofsetupofestablishment = models.CharField(max_length=250, blank=True, null=True)
    address = models.CharField(max_length=250, blank=True, null=True)
    pincode = models.BigIntegerField(blank=True, null=True)
    city = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    pfstate = models.CharField(max_length=250, blank=True, null=True)
    country = models.CharField(max_length=250, blank=True, null=True)
    epfoofficename = models.CharField(max_length=250, blank=True, null=True)
    epfoofficeaddress = models.CharField(max_length=250, blank=True, null=True)
    pfzone = models.CharField(max_length=250, blank=True, null=True)
    region = models.CharField(max_length=250, blank=True, null=True)
    subcode = models.CharField(max_length=250, blank=True, null=True)
    othercodename = models.CharField(max_length=250, blank=True, null=True)
    othercodedesignation = models.CharField(max_length=250, blank=True, null=True)
    othercodedateofbirth = models.CharField(max_length=250, blank=True, null=True)
    othercodefathername = models.CharField(max_length=250, blank=True, null=True)
    othercoderesidentialaddress = models.CharField(max_length=250, blank=True, null=True)
    othercodedateposition = models.CharField(max_length=250, blank=True, null=True)
    branch = models.CharField(max_length=250, blank=True, null=True)
    estwithsamepan = models.CharField(max_length=250, blank=True, null=True)
    otherdetails1 = models.CharField(max_length=250, blank=True, null=True)
    otherdetails2 = models.CharField(max_length=250, blank=True, null=True)
    cincode = models.CharField(max_length=250, blank=True, null=True)
    lincode = models.CharField(max_length=250, blank=True, null=True)
    startuporderno = models.CharField(max_length=250, blank=True, null=True)
    startuporderdate = models.CharField(max_length=250, blank=True, null=True)
    msmeorderno = models.CharField(max_length=250, blank=True, null=True)
    msmeorderdate = models.CharField(max_length=250, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_pf_details'


class UplaudsPfPaymentDetails(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    pfpaymentdetailsid = models.BigIntegerField(primary_key=True)
    cin = models.CharField(max_length=75, blank=True, null=True)
    establishmentid = models.CharField(max_length=75, blank=True, null=True)
    trn = models.BigIntegerField(blank=True, null=True)
    dateofcredit = models.CharField(max_length=75, blank=True, null=True)
    amount = models.DecimalField(max_digits=30, decimal_places=16, blank=True, null=True)
    wagemonth = models.CharField(max_length=150, blank=True, null=True)
    noofemployees = models.BigIntegerField(blank=True, null=True)
    ecr = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_pf_payment_details'


class UplaudsSearchData(models.Model):
    name = models.TextField(blank=True, null=True)
    cin = models.TextField(unique=True, blank=True, null=True)
    name_cin_vector = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'uplauds_search_data'


class UplaudsViCorpDetails(models.Model):
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    vi_corp_id = models.BigIntegerField(primary_key=True)
    cin = models.BigIntegerField(blank=True, null=True)
    auditor_name = models.BigIntegerField(blank=True, null=True)
    business_desc = models.CharField(max_length=75, blank=True, null=True)
    city = models.CharField(max_length=75, blank=True, null=True)
    corp_name = models.CharField(max_length=75, blank=True, null=True)
    corp_status = models.CharField(max_length=75, blank=True, null=True)
    country = models.CharField(max_length=75, blank=True, null=True)
    contact_name = models.CharField(max_length=75, blank=True, null=True)
    designation = models.CharField(max_length=75, blank=True, null=True)
    email = models.CharField(max_length=75, blank=True, null=True)
    industry = models.CharField(max_length=75, blank=True, null=True)
    sector = models.CharField(max_length=75, blank=True, null=True)
    status = models.CharField(max_length=75, blank=True, null=True)
    telephone = models.CharField(max_length=75, blank=True, null=True)
    transaction_status = models.CharField(max_length=75, blank=True, null=True)
    website = models.CharField(max_length=75, blank=True, null=True)
    year_founded = models.CharField(max_length=75, blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uplauds_vi_corp_details'
        unique_together = (('uuid_field', 'groupid'),)


class User(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    externalreferencecode = models.CharField(max_length=75, blank=True, null=True)
    userid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    defaultuser = models.BooleanField(blank=True, null=True)
    contactid = models.BigIntegerField(unique=True, blank=True, null=True)
    password_field = models.CharField(db_column='password_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    passwordencrypted = models.BooleanField(blank=True, null=True)
    passwordreset = models.BooleanField(blank=True, null=True)
    passwordmodifieddate = models.DateTimeField(blank=True, null=True)
    digest = models.CharField(max_length=255, blank=True, null=True)
    reminderqueryquestion = models.CharField(max_length=75, blank=True, null=True)
    reminderqueryanswer = models.CharField(max_length=75, blank=True, null=True)
    gracelogincount = models.IntegerField(blank=True, null=True)
    screenname = models.CharField(max_length=75, blank=True, null=True)
    emailaddress = models.CharField(max_length=254, blank=True, null=True)
    facebookid = models.BigIntegerField(blank=True, null=True)
    googleuserid = models.CharField(max_length=75, blank=True, null=True)
    ldapserverid = models.BigIntegerField(blank=True, null=True)
    openid = models.CharField(max_length=1024, blank=True, null=True)
    portraitid = models.BigIntegerField(blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)
    timezoneid = models.CharField(max_length=75, blank=True, null=True)
    greeting = models.CharField(max_length=255, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    firstname = models.CharField(max_length=75, blank=True, null=True)
    middlename = models.CharField(max_length=75, blank=True, null=True)
    lastname = models.CharField(max_length=75, blank=True, null=True)
    jobtitle = models.CharField(max_length=100, blank=True, null=True)
    logindate = models.DateTimeField(blank=True, null=True)
    loginip = models.CharField(max_length=75, blank=True, null=True)
    lastlogindate = models.DateTimeField(blank=True, null=True)
    lastloginip = models.CharField(max_length=75, blank=True, null=True)
    lastfailedlogindate = models.DateTimeField(blank=True, null=True)
    failedloginattempts = models.IntegerField(blank=True, null=True)
    lockout = models.BooleanField(blank=True, null=True)
    lockoutdate = models.DateTimeField(blank=True, null=True)
    agreedtotermsofuse = models.BooleanField(blank=True, null=True)
    emailaddressverified = models.BooleanField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_'
        unique_together = (('companyid', 'emailaddress'), ('companyid', 'userid'), ('companyid', 'screenname'),)


class Usergroup(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    externalreferencecode = models.CharField(max_length=75, blank=True, null=True)
    usergroupid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    parentusergroupid = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    addedbyldapimport = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usergroup'
        unique_together = (('companyid', 'name'),)


class Usergroupgrouprole(models.Model):
    mvccversion = models.BigIntegerField()
    usergroupgrouproleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    usergroupid = models.BigIntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    roleid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usergroupgrouprole'
        unique_together = (('usergroupid', 'groupid', 'roleid'),)


class Usergrouprole(models.Model):
    mvccversion = models.BigIntegerField()
    usergrouproleid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    roleid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usergrouprole'
        unique_together = (('userid', 'groupid', 'roleid'),)


class UsergroupsTeams(models.Model):
    companyid = models.BigIntegerField()
    teamid = models.BigIntegerField(primary_key=True)
    usergroupid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'usergroups_teams'
        unique_together = (('teamid', 'usergroupid'),)


class Useridmapper(models.Model):
    mvccversion = models.BigIntegerField()
    useridmapperid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    description = models.CharField(max_length=75, blank=True, null=True)
    externaluserid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'useridmapper'
        unique_together = (('type_field', 'externaluserid'), ('userid', 'type_field'),)


class Usernotificationdelivery(models.Model):
    mvccversion = models.BigIntegerField()
    usernotificationdeliveryid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    portletid = models.CharField(max_length=200, blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    notificationtype = models.IntegerField(blank=True, null=True)
    deliverytype = models.IntegerField(blank=True, null=True)
    deliver = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usernotificationdelivery'
        unique_together = (('userid', 'portletid', 'classnameid', 'notificationtype', 'deliverytype'),)


class Usernotificationevent(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    usernotificationeventid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    type_field = models.CharField(db_column='type_', max_length=200, blank=True, null=True)  # Field renamed because it ended with '_'.
    timestamp = models.BigIntegerField(blank=True, null=True)
    deliverytype = models.IntegerField(blank=True, null=True)
    deliverby = models.BigIntegerField(blank=True, null=True)
    delivered = models.BooleanField(blank=True, null=True)
    payload = models.TextField(blank=True, null=True)
    actionrequired = models.BooleanField(blank=True, null=True)
    archived = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usernotificationevent'


class UsersGroups(models.Model):
    companyid = models.BigIntegerField()
    groupid = models.BigIntegerField(primary_key=True)
    userid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'users_groups'
        unique_together = (('groupid', 'userid'),)


class UsersOrgs(models.Model):
    companyid = models.BigIntegerField()
    organizationid = models.BigIntegerField(primary_key=True)
    userid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'users_orgs'
        unique_together = (('organizationid', 'userid'),)


class UsersRoles(models.Model):
    companyid = models.BigIntegerField()
    roleid = models.BigIntegerField(primary_key=True)
    userid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'users_roles'
        unique_together = (('roleid', 'userid'),)


class UsersTeams(models.Model):
    companyid = models.BigIntegerField()
    teamid = models.BigIntegerField(primary_key=True)
    userid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'users_teams'
        unique_together = (('teamid', 'userid'),)


class UsersUsergroups(models.Model):
    companyid = models.BigIntegerField()
    userid = models.BigIntegerField(primary_key=True)
    usergroupid = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'users_usergroups'
        unique_together = (('userid', 'usergroupid'),)


class Usertracker(models.Model):
    mvccversion = models.BigIntegerField()
    usertrackerid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    sessionid = models.CharField(max_length=200, blank=True, null=True)
    remoteaddr = models.CharField(max_length=75, blank=True, null=True)
    remotehost = models.CharField(max_length=75, blank=True, null=True)
    useragent = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usertracker'


class Usertrackerpath(models.Model):
    mvccversion = models.BigIntegerField()
    usertrackerpathid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    usertrackerid = models.BigIntegerField(blank=True, null=True)
    path_field = models.TextField(db_column='path_', blank=True, null=True)  # Field renamed because it ended with '_'.
    pathdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usertrackerpath'


class Viewcountentry(models.Model):
    companyid = models.BigIntegerField(primary_key=True)
    classnameid = models.BigIntegerField()
    classpk = models.BigIntegerField()
    viewcount = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'viewcountentry'
        unique_together = (('companyid', 'classnameid', 'classpk'),)


class Virtualhost(models.Model):
    mvccversion = models.BigIntegerField()
    virtualhostid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    layoutsetid = models.BigIntegerField(blank=True, null=True)
    hostname = models.CharField(unique=True, max_length=200, blank=True, null=True)
    defaultvirtualhost = models.BooleanField(blank=True, null=True)
    languageid = models.CharField(max_length=75, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'virtualhost'


class Webdavprops(models.Model):
    mvccversion = models.BigIntegerField()
    webdavpropsid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    props = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'webdavprops'
        unique_together = (('classnameid', 'classpk'),)


class Website(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    websiteid = models.BigIntegerField(primary_key=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    typeid = models.BigIntegerField(blank=True, null=True)
    primary_field = models.BooleanField(db_column='primary_', blank=True, null=True)  # Field renamed because it ended with '_'.
    lastpublishdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'website'


class Wikinode(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    nodeid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    lastpostdate = models.DateTimeField(blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wikinode'
        unique_together = (('uuid_field', 'groupid'), ('groupid', 'name'),)


class Wikipage(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    pageid = models.BigIntegerField(primary_key=True)
    resourceprimkey = models.BigIntegerField(blank=True, null=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    nodeid = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    version = models.FloatField(blank=True, null=True)
    minoredit = models.BooleanField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=75, blank=True, null=True)
    head = models.BooleanField(blank=True, null=True)
    parenttitle = models.CharField(max_length=255, blank=True, null=True)
    redirecttitle = models.CharField(max_length=255, blank=True, null=True)
    lastpublishdate = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    statusbyuserid = models.BigIntegerField(blank=True, null=True)
    statusbyusername = models.CharField(max_length=75, blank=True, null=True)
    statusdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wikipage'
        unique_together = (('resourceprimkey', 'nodeid', 'version'), ('nodeid', 'title', 'version'), ('uuid_field', 'groupid'),)


class Wikipageresource(models.Model):
    mvccversion = models.BigIntegerField()
    uuid_field = models.CharField(db_column='uuid_', max_length=75, blank=True, null=True)  # Field renamed because it ended with '_'.
    resourceprimkey = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    nodeid = models.BigIntegerField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wikipageresource'
        unique_together = (('nodeid', 'title'), ('uuid_field', 'groupid'),)


class Workflowdefinitionlink(models.Model):
    mvccversion = models.BigIntegerField()
    workflowdefinitionlinkid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    typepk = models.BigIntegerField(blank=True, null=True)
    workflowdefinitionname = models.CharField(max_length=75, blank=True, null=True)
    workflowdefinitionversion = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'workflowdefinitionlink'


class Workflowinstancelink(models.Model):
    mvccversion = models.BigIntegerField()
    workflowinstancelinkid = models.BigIntegerField(primary_key=True)
    groupid = models.BigIntegerField(blank=True, null=True)
    companyid = models.BigIntegerField(blank=True, null=True)
    userid = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=75, blank=True, null=True)
    createdate = models.DateTimeField(blank=True, null=True)
    modifieddate = models.DateTimeField(blank=True, null=True)
    classnameid = models.BigIntegerField(blank=True, null=True)
    classpk = models.BigIntegerField(blank=True, null=True)
    workflowinstanceid = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'workflowinstancelink'
